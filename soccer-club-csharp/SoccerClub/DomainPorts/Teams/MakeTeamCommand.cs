namespace SoccerClub.DomainPorts.Teams;

public class MakeTeamCommand
{
    public int ClubNumber { get; }
    public Guid TeamId { get; }
    public string Name { get; }

    public MakeTeamCommand(int clubNumber, Guid teamId, string name)
    {
        ClubNumber = clubNumber;
        TeamId = teamId;
        Name = name;
    }
}