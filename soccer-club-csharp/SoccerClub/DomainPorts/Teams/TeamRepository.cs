using SoccerClub.Domain.Teams;

namespace SoccerClub.DomainPorts.Teams;

public interface TeamRepository
{
    // Retrieves a team for a specified club and identifier.
    // When found, returns a `Team` instance; otherwise a null reference is returned.
    Team? Get(int clubNumber, Guid id);

    // Generates an identifier for a new `Team` instance.
    Guid GetNextIdentity();

    // Saves the data of the specified `Team` instance.
    void Save(Team team);    
}