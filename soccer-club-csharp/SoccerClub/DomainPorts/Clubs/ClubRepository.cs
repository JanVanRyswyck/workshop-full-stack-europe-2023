using SoccerClub.Domain.Clubs;

namespace SoccerClub.DomainPorts.Clubs;

public interface ClubRepository
{
    // Retrieves a club for a specified master number.
    // When found, returns a `Club` instance; otherwise a null reference is returned.
    Club? Get(int masterNumber);

    // Saves the data of the specified `Club` instance.
    void Save(Club club);
}