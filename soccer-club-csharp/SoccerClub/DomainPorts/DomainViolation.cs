namespace SoccerClub.DomainPorts;

public class DomainViolation
{
    public string Message { get; }
        
    public DomainViolation(string message)
    {
        Message = message;
    }
}