using SoccerClub.Domain.ClubMembers;

namespace SoccerClub.DomainPorts.ClubMembers;

public interface ClubMemberNotifier
{
    void SendPaymentRequestForMembershipFee(ClubMember clubMember);
    void SendCongratulationsForCertification(ClubMember clubMember);
}