using NodaTime;

namespace SoccerClub.DomainPorts.ClubMembers;

public class RegisterClubMemberCommand
{
    public int ClubNumber { get; }
    public Guid ClubMemberId { get; }
    public string FirstName { get; }
    public string LastName { get; }
    public string Email { get; }
    public LocalDate BirthDate { get; }
    public string NationalIdentificationNumber { get; }
    
    public RegisterClubMemberCommand(int clubNumber, Guid clubMemberId, string firstName, string lastName, string email,
                                     LocalDate birthDate, string nationalIdentificationNumber) {
        ClubNumber = clubNumber;
        ClubMemberId = clubMemberId;
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        BirthDate = birthDate;
        NationalIdentificationNumber = nationalIdentificationNumber;
    }
}