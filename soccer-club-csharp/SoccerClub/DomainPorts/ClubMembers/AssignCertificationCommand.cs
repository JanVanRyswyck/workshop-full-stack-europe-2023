using NodaTime;

namespace SoccerClub.DomainPorts.ClubMembers;

public class AssignCertificationCommand
{
    public int ClubNumber { get; }
    public Guid ClubMemberId { get; }
    public string CertificateCode { get; }
    public LocalDate IssueDate { get; }
    
    public AssignCertificationCommand(int clubNumber, Guid clubMemberId, string certificateCode, LocalDate issueDate)
    {
        ClubNumber = clubNumber;
        ClubMemberId = clubMemberId;
        CertificateCode = certificateCode;
        IssueDate = issueDate;
    }
}