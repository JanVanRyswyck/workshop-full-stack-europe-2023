using SoccerClub.Domain.ClubMembers;

namespace SoccerClub.DomainPorts.ClubMembers;

public interface ClubMemberRepository
{
    // Retrieves a club member for a specified club and identifier.
    // When found, returns a `ClubMember` instance; otherwise a null reference is returned.
    ClubMember? Get(int clubNumber, Guid id);

    // Retrieves a club member for a specified club and national identification number.
    // When found, returns a `ClubMember` instance; otherwise a null reference is returned.
    ClubMember? Get(int clubNumber, String nationalIdentificationNumber);

    // Generates an identifier for a new `ClubMember` instance.
    Guid GetNextIdentity();

    // Saves the data of the specified `ClubMember` instance.
    void Save(ClubMember clubMember);
}