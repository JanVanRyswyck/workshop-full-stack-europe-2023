namespace SoccerClub.DomainPorts;

public interface CommandHandler<TCommand> 
{
    Result Handle(TCommand command);
}