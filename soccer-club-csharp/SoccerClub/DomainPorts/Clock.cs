using NodaTime;

namespace SoccerClub.DomainPorts;

public interface Clock
{
    LocalDate GetCurrentDate();
}