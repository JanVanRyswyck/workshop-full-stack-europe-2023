namespace SoccerClub.DomainPorts;

public class Result
{
    public bool IsSuccessful { get; }
    public IEnumerable<DomainViolation> DomainViolations { get; }

    private Result(IEnumerable<DomainViolation> domainViolations)
    {
        DomainViolations = domainViolations;
        IsSuccessful = ! DomainViolations.Any();
    }
        
    public static Result Success()
    {
        return new Result(Enumerable.Empty<DomainViolation>());
    }

    public static Result Failure(params DomainViolation[] domainViolations)
    {
        return new Result(domainViolations);
    }
}