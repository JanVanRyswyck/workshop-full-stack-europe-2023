namespace SoccerClub.Api.Teams;

public class MakeTeamRequest
{
    public string? Name { get; init; }
}