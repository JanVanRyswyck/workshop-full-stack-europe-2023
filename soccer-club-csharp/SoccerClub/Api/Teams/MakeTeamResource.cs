using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.Teams;

namespace SoccerClub.Api.Teams;

[ApiController]
[Route("/api/clubs/{clubNumber}/teams")]
public class MakeTeamResource : Controller
{
    private readonly TeamRepository _repository;
    private readonly CommandHandler<MakeTeamCommand> _commandHandler;
    private readonly MakeTeamRequestValidator _validator;

    public MakeTeamResource(TeamRepository repository,
        CommandHandler<MakeTeamCommand> commandHandler)
    {
        _repository = repository;
        _commandHandler = commandHandler;
        _validator = new MakeTeamRequestValidator();
    }

    [HttpPost]
    public ActionResult MakeTeam(int clubNumber, MakeTeamRequest request)
    {
        var requestIsValid = _validator.Validate(request);
        if(! requestIsValid)
            return BadRequest();

        var newTeamId = _repository.GetNextIdentity();
        var command = new MakeTeamCommand(clubNumber, newTeamId, request.Name!);

        var result = _commandHandler.Handle(command);
        if(! result.IsSuccessful)
            return BadRequest();

        var response = new MakeTeamResponse { TeamId = newTeamId };
        return Ok(response);
    }
}