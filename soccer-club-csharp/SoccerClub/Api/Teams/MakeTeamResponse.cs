namespace SoccerClub.Api.Teams;

public class MakeTeamResponse
{
    public Guid TeamId { get; init; }
}