namespace SoccerClub.Api.Teams;

public class MakeTeamRequestValidator
{
    public bool Validate(MakeTeamRequest request) {
        return HasValidName(request);
    }

    private static bool HasValidName(MakeTeamRequest request) {
        return !string.IsNullOrWhiteSpace(request.Name);
    }        
}