using System.Text.RegularExpressions;

namespace SoccerClub.Api.ClubMembers;

public class RegisterClubMemberRequestValidator
{
    private const string NationalIdentificationNumberPattern = "^\\d{11}$";

    public bool Validate(RegisterClubMemberRequest request) 
    {
        return HasValidFirstName(request) &&
               HasValidLastName(request) &&
               HasValidEmail(request) &&
               HasValidBirthDate(request) &&
               HasValidNationalIdentificationNumber(request);
    }

    private static bool HasValidFirstName(RegisterClubMemberRequest request)
    {
        return !string.IsNullOrWhiteSpace(request.FirstName);
    }

    private static bool HasValidLastName(RegisterClubMemberRequest request) 
    {
        return !string.IsNullOrWhiteSpace(request.LastName);
    }

    private static bool HasValidEmail(RegisterClubMemberRequest request) 
    {
        return !string.IsNullOrWhiteSpace(request.Email);
    }

    private static bool HasValidBirthDate(RegisterClubMemberRequest request) 
    {
        return request.BirthDate != null;
    }

    private static bool HasValidNationalIdentificationNumber(RegisterClubMemberRequest request) {
        var birthDateSegment = request.BirthDate.GetValueOrDefault().ToString("yyMMdd");
        
        return !string.IsNullOrWhiteSpace(request.NationalIdentificationNumber) &&
               Regex.IsMatch(request.NationalIdentificationNumber, NationalIdentificationNumberPattern) &&
               request.NationalIdentificationNumber.StartsWith(birthDateSegment);
    }
}