using Microsoft.AspNetCore.Mvc;
using NodaTime;
using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.ClubMembers;

namespace SoccerClub.Api.ClubMembers;

[ApiController]
[Route("/api/clubs/{clubNumber:int}/members/{clubMemberId:guid}/certifications")]
public class AssignCertificationResource : Controller
{
    private readonly CommandHandler<AssignCertificationCommand> _commandHandler;
    private readonly AssignCertificationRequestValidator _validator;

    public AssignCertificationResource(CommandHandler<AssignCertificationCommand> commandHandler)
    {
        _commandHandler = commandHandler;
        _validator = new AssignCertificationRequestValidator();
    }
    
    [HttpPut]
    public ActionResult AssignCertification(int clubNumber, Guid clubMemberId, AssignCertificationRequest request)
    {
        var requestIsValid = _validator.Validate(request);
        if(! requestIsValid)
            return BadRequest();

        var issueDate = LocalDate.FromDateTime(request.IssueDate.GetValueOrDefault());
        var command = new AssignCertificationCommand(clubNumber, clubMemberId, request.CertificateCode!, issueDate);

        var result = _commandHandler.Handle(command);
        if(! result.IsSuccessful)
            return BadRequest();

        return Ok();
    }
}