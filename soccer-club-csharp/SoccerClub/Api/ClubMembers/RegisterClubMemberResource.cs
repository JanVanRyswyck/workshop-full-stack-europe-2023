using Microsoft.AspNetCore.Mvc;
using NodaTime;
using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.ClubMembers;

namespace SoccerClub.Api.ClubMembers;

[ApiController]
[Route("/api/clubs/{clubNumber:int}/members")]
public class RegisterClubMemberResource : Controller
{
    private readonly ClubMemberRepository _repository;
    private readonly CommandHandler<RegisterClubMemberCommand> _commandHandler;
    private readonly RegisterClubMemberRequestValidator _validator;
    
    public RegisterClubMemberResource(ClubMemberRepository repository,
        CommandHandler<RegisterClubMemberCommand> commandHandler)
    {
        _repository = repository;
        _commandHandler = commandHandler;
        _validator = new RegisterClubMemberRequestValidator();
    }
    
    [HttpPost]
    public ActionResult RegisterClubMember(int clubNumber, RegisterClubMemberRequest request)
    {
        var requestIsValid = _validator.Validate(request);
        if(! requestIsValid)
            return BadRequest();
        
        var newClubMemberId = _repository.GetNextIdentity();
        var birthDate = LocalDate.FromDateTime(request.BirthDate.GetValueOrDefault());
        var command = new RegisterClubMemberCommand(clubNumber, newClubMemberId, request.FirstName!, request.LastName!,
            request.Email!, birthDate, request.NationalIdentificationNumber!);
        
        var result = _commandHandler.Handle(command);
        if(! result.IsSuccessful)
            return BadRequest();
        
        var response = new RegisterClubMemberResponse { ClubMemberId = newClubMemberId };
        return Ok(response);
    }        
}