namespace SoccerClub.Api.ClubMembers;

public class AssignCertificationRequest
{
    public string? CertificateCode { get; init; }
    public DateTime? IssueDate { get; init; }
}