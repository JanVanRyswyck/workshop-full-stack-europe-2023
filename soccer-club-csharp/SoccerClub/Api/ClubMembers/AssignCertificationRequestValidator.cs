namespace SoccerClub.Api.ClubMembers;

public class AssignCertificationRequestValidator
{
    public bool Validate(AssignCertificationRequest request)
    {
        return HasValidCertificateCode(request) &&
               HasValidIssueDate(request);
    }
    
    private static bool HasValidCertificateCode(AssignCertificationRequest request) 
    {
        return !string.IsNullOrWhiteSpace(request.CertificateCode);
    }

    private static bool HasValidIssueDate(AssignCertificationRequest request) 
    {
        return request.IssueDate != null;
    }
}