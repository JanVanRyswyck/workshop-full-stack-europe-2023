namespace SoccerClub.Api.ClubMembers;

public class RegisterClubMemberResponse
{
    public Guid ClubMemberId { get; init; }
}