namespace SoccerClub.Api.ClubMembers;

public class RegisterClubMemberRequest
{
    public string? FirstName { get; init; }     
    public string? LastName { get; init; }     
    public string? Email { get; init; }     
    public DateTime? BirthDate { get; init; }     
    public string? NationalIdentificationNumber { get; init; }     
}