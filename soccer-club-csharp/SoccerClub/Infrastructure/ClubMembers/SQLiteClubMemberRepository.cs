using System.Data;
using System.Reflection;
using Microsoft.Data.Sqlite;
using NodaTime;
using SoccerClub.Domain.ClubMembers;
using SoccerClub.Domain.Clubs;
using SoccerClub.DomainPorts.ClubMembers;

namespace SoccerClub.Infrastructure.ClubMembers;

public class SQLiteClubMemberRepository : ClubMemberRepository
{
    private readonly string _connectionString;

    public SQLiteClubMemberRepository(ConfigurationSettings configurationSettings)
    {
        _connectionString = configurationSettings.GetConnectionString();
    }
    
    public ClubMember? Get(int clubNumber, Guid id)
    {
        const string sql = "SELECT Id, ClubNumber, FirstName, LastName, Email, BirthDate, NationalIdentificationNumber " +
            "FROM ClubMember " +
            "WHERE Id = @id " +
            "AND ClubNumber = @clubNumber";
        
        using var connection = new SqliteConnection(_connectionString);
        using var command = new SqliteCommand(sql, connection);

        command.Parameters.AddWithValue("@id", id);
        command.Parameters.AddWithValue("@clubNumber", clubNumber);
        
        return QueryClubMember(connection, command);
    }

    public ClubMember? Get(int clubNumber, string nationalIdentificationNumber)
    {
        const string sql = "SELECT Id, ClubNumber, FirstName, LastName, Email, BirthDate, NationalIdentificationNumber " +
            "FROM ClubMember " +
            "WHERE NationalIdentificationNumber = @nationalIdentificationNumber " +
            "AND ClubNumber = @clubNumber";
        
        using var connection = new SqliteConnection(_connectionString);
        using var command = new SqliteCommand(sql, connection);
        
        command.Parameters.AddWithValue("@nationalIdentificationNumber", nationalIdentificationNumber);
        command.Parameters.AddWithValue("@clubNumber", clubNumber);
        
        return QueryClubMember(connection, command);
    }

    public Guid GetNextIdentity()
    {
        return Guid.NewGuid();
    }

    public void Save(ClubMember clubMember)
    {
        if(clubMember.DomainViolations.Any())
            throw new InvalidOperationException("The specified club member cannot be saved due to domain violations.");
        
        using var connection = new SqliteConnection(_connectionString);
        connection.Open();
        
        if(Exists(clubMember, connection)) 
            Update(clubMember, connection);
        else 
            Insert(clubMember, connection);
    }

    private static ClubMember? QueryClubMember(SqliteConnection connection, IDbCommand command)
    {
        connection.Open();
        var dataReader = command.ExecuteReader();
        
        var result = MapClubMemberFrom(dataReader);
        if(result == null)
            return null;
        
        PopulateWithCertifications(result, connection);
        return result;
    }

    private static ClubMember? MapClubMemberFrom(IDataReader dataReader)
    {
        if(! dataReader.Read())
            return null;

        var clubProxy = new Club(dataReader.GetInt32(dataReader.GetOrdinal("ClubNumber")), null);
        var birthDate = LocalDate.FromDateTime(dataReader.GetDateTime(dataReader.GetOrdinal("BirthDate")));
        
        return new ClubMember(
            dataReader.GetGuid(dataReader.GetOrdinal("Id")),
            clubProxy,
            dataReader.GetString(dataReader.GetOrdinal("FirstName")),
            dataReader.GetString(dataReader.GetOrdinal("LastName")),
            dataReader.GetString(dataReader.GetOrdinal("Email")),
            birthDate,
            dataReader.GetString(dataReader.GetOrdinal("NationalIdentificationNumber"))
        );
    }

    private static void PopulateWithCertifications(ClubMember clubMember, SqliteConnection connection)
    {
        const string sql = "SELECT Code, IssueDate FROM ClubMemberCertification WHERE ClubMemberId = @clubMemberId";
        
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@clubMemberId", clubMember.Id);

        var dataReader = command.ExecuteReader();

        var certifications = new List<Certification>();
        while(dataReader.Read())
        {
            certifications.Add(MapCertificationFrom(dataReader));        
        }

        var certificationsField = clubMember.GetType()
            .GetField("_certifications", BindingFlags.Instance | BindingFlags.NonPublic);
        if(null == certificationsField)
            throw new InvalidOperationException("Unable to locate field '_certifications' for type 'ClubMember'.");
        
        certificationsField.SetValue(clubMember, certifications);
    }

    private static Certification MapCertificationFrom(IDataRecord dataRecord)
    {
        return new Certification(
            dataRecord.GetString(dataRecord.GetOrdinal("Code")),
            LocalDate.FromDateTime(dataRecord.GetDateTime(dataRecord.GetOrdinal("IssueDate")))
        );
    }

    private static bool Exists(ClubMember clubMember, SqliteConnection connection)
    {
        const string sql = "SELECT 1 FROM ClubMember WHERE Id = @id AND ClubNumber = @clubNumber";
        
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@id", clubMember.Id);
        command.Parameters.AddWithValue("@clubNumber", clubMember.ClubNumber);

        var result = command.ExecuteScalar();
        return result != null && ((long)result) == 1;
    }

    private static void Insert(ClubMember clubMember, SqliteConnection connection)
    {
        const string sql = "INSERT INTO ClubMember (Id, ClubNumber, FirstName, LastName, Email, BirthDate, NationalIdentificationNumber) " +
            "VALUES(@id, @clubNumber, @firstName, @lastName, @email, @birthDate, @nationalIdentificationNumber)";
        
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@id", clubMember.Id);
        command.Parameters.AddWithValue("@clubNumber", clubMember.ClubNumber);
        command.Parameters.AddWithValue("@firstName", clubMember.FirstName);
        command.Parameters.AddWithValue("@lastName", clubMember.LastName);
        command.Parameters.AddWithValue("@email", clubMember.Email);
        command.Parameters.AddWithValue("@birthDate", clubMember.BirthDate.ToDateTimeUnspecified());
        command.Parameters.AddWithValue("@nationalIdentificationNumber", clubMember.NationalIdentificationNumber);

        command.ExecuteNonQuery();
    }

    private static void Update(ClubMember clubMember, SqliteConnection connection)
    {
        const string sql = "UPDATE ClubMember " +
            "SET FirstName = @firstName, " +
            "LastName = @lastName, " +
            "Email = @email, " +
            "BirthDate = @birthDate, " +
            "NationalIdentificationNumber = @nationalIdentificationNumber " +
            "WHERE Id = @id AND ClubNumber = @clubNumber";
        
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@firstName", clubMember.FirstName);
        command.Parameters.AddWithValue("@lastName", clubMember.LastName);
        command.Parameters.AddWithValue("@email", clubMember.Email);
        command.Parameters.AddWithValue("@birthDate", clubMember.BirthDate.ToDateTimeUnspecified());
        command.Parameters.AddWithValue("@nationalIdentificationNumber", clubMember.NationalIdentificationNumber);
        command.Parameters.AddWithValue("@id", clubMember.Id);
        command.Parameters.AddWithValue("@clubNumber", clubMember.ClubNumber);
        
        command.ExecuteNonQuery();
        
        RemoveAllCertifications(clubMember, connection);
        clubMember.Certifications.ToList()
            .ForEach(certification => Insert(certification, clubMember.Id, clubMember.ClubNumber, connection));
    }

    private static void RemoveAllCertifications(ClubMember clubMember, SqliteConnection connection)
    {
        const string sql = "DELETE FROM ClubMemberCertification WHERE ClubMemberId = @clubMemberId AND ClubNumber = @clubNumber";
        
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@clubMemberId", clubMember.Id);
        command.Parameters.AddWithValue("@clubNumber", clubMember.ClubNumber);

        command.ExecuteNonQuery();
    }

    private static void Insert(Certification certification, Guid clubMemberId, int clubNumber, SqliteConnection connection)
    {
        const string sql = "INSERT INTO ClubMemberCertification (Code, ClubMemberId, ClubNumber, IssueDate)" +
            "VALUES(@code, @clubMemberId, @clubNumber, @issueDate)";
        
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@code", certification.CertificateCode);
        command.Parameters.AddWithValue("@clubMemberId", clubMemberId);
        command.Parameters.AddWithValue("@clubNumber", clubNumber);
        command.Parameters.AddWithValue("@issueDate", certification.IssueDate.ToDateTimeUnspecified());

        command.ExecuteNonQuery();
    }
}