namespace SoccerClub.Infrastructure;

public interface ConfigurationSettings
{
    string GetConnectionString();
}

public class ApplicationConfigurationSettings : ConfigurationSettings
{
    private readonly IConfiguration _configuration;

    public ApplicationConfigurationSettings(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    
    public string GetConnectionString()
    {
        return _configuration.GetConnectionString("DefaultConnection") ?? string.Empty;
    }
}