using System.Data;
using Microsoft.Data.Sqlite;
using SoccerClub.Domain.Clubs;
using SoccerClub.Domain.Teams;
using SoccerClub.DomainPorts.Teams;

namespace SoccerClub.Infrastructure.Teams;

public class SQLiteTeamRepository : TeamRepository
{
    private readonly string _connectionString;
    
    public SQLiteTeamRepository(ConfigurationSettings configurationSettings)
    {
        _connectionString = configurationSettings.GetConnectionString();    
    }
    
    public Team? Get(int clubNumber, Guid id)
    {
        const string sql = "SELECT Id, ClubNumber, Name, SeasonStartYear " +
            "FROM Team " +
            "WHERE Id = @id " +
            "AND ClubNumber = @clubNumber";
        
        using var connection = new SqliteConnection(_connectionString);
        using var command = new SqliteCommand(sql, connection);
        
        command.Parameters.AddWithValue("@id", id);
        command.Parameters.AddWithValue("@clubNumber", clubNumber);
        
        var dataReader = command.ExecuteReader();
        return MapFrom(dataReader);
    }

    private static Team? MapFrom(IDataReader dataReader)
    {
        if(! dataReader.Read())
            return null;

        var clubProxy = new Club(dataReader.GetInt32(dataReader.GetOrdinal("ClubNumber")), null);
        var season = new Season(dataReader.GetInt32(dataReader.GetOrdinal("SeasonStartYear")));
        
        return new Team(
            dataReader.GetGuid(dataReader.GetOrdinal("Id")),
            clubProxy,
            dataReader.GetString(dataReader.GetOrdinal("Name")),
            season
        );
    }

    public Guid GetNextIdentity()
    {
        return Guid.NewGuid();
    }

    public void Save(Team team)
    {
        using var connection = new SqliteConnection(_connectionString);
        connection.Open();
        
        if(Exists(team, connection)) 
            Update(team, connection);
        else 
            Insert(team, connection);
    }

    private static bool Exists(Team team, SqliteConnection connection)
    {
        const string sql = "SELECT 1 FROM Team WHERE Id = @id AND ClubNumber = @clubNumber";
        
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@id", team.Id);
        command.Parameters.AddWithValue("@clubNumber", team.ClubNumber);
        
        var result = command.ExecuteScalar();
        return result != null && ((long)result) == 1;
    }

    private static void Insert(Team team, SqliteConnection connection)
    {
        const string sql = "INSERT INTO Team (Id, ClubNumber, Name, SeasonStartYear) " +
            "VALUES(@id, @clubNumber, @name, @seasonStartYear)";    
        
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@id", team.Id);
        command.Parameters.AddWithValue("@clubNumber", team.ClubNumber);
        command.Parameters.AddWithValue("@name", team.Name);
        command.Parameters.AddWithValue("@seasonStartYear", team.Season.StartYear);
        
        command.ExecuteNonQuery();
    }

    private static void Update(Team team, SqliteConnection connection)
    {
        const string sql = "UPDATE Team " +
            "SET Name = @name, " +
            "SeasonStartYear = @seasonStartYear " +
            "WHERE Id = @id AND ClubNumber = @clubNumber";
        
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@name", team.Name);
        command.Parameters.AddWithValue("@seasonStartYear", team.Season.StartYear);
        command.Parameters.AddWithValue("@id", team.Id);
        command.Parameters.AddWithValue("@clubNumber", team.ClubNumber);
        
        command.ExecuteNonQuery();
    }
}