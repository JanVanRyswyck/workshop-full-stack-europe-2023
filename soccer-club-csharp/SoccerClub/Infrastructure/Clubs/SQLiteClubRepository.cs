using System.Data;
using Microsoft.Data.Sqlite;
using SoccerClub.Domain.Clubs;
using SoccerClub.DomainPorts.Clubs;

namespace SoccerClub.Infrastructure.Clubs;

public class SQLiteClubRepository : ClubRepository
{
    private readonly string _connectionString;
    
    public SQLiteClubRepository(ConfigurationSettings configurationSettings)
    {
        _connectionString = configurationSettings.GetConnectionString();
    }
    
    public Club? Get(int masterNumber)
    {
        const string sql = "SELECT MasterNumber, Name FROM Club WHERE MasterNumber = @masterNumber";

        using var connection = new SqliteConnection(_connectionString);
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@masterNumber", masterNumber);
        
        connection.Open();
        var dataReader = command.ExecuteReader();
        return MapFrom(dataReader);
    }

    private static Club? MapFrom(IDataReader dataReader)
    {
        if(! dataReader.Read())
            return null;

        return new Club(
            dataReader.GetInt32(dataReader.GetOrdinal("MasterNumber")),
            dataReader.GetString(dataReader.GetOrdinal("Name"))
        );
    }

    public void Save(Club club)
    {
        const string sql = "INSERT INTO Club (MasterNumber, Name) VALUES(@masterNumber, @name)";
        
        using var connection = new SqliteConnection(_connectionString);
        using var command = new SqliteCommand(sql, connection);

        command.Parameters.AddWithValue("@masterNumber", club.MasterNumber);
        command.Parameters.AddWithValue("@name", club.Name);
        
        connection.Open();
        command.ExecuteNonQuery();
    }
}