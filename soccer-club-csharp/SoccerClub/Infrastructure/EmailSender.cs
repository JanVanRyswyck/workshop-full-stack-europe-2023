using SoccerClub.Domain.ClubMembers;
using SoccerClub.DomainPorts.ClubMembers;

namespace SoccerClub.Infrastructure;

public class EmailSender : ClubMemberNotifier
{
    public void SendPaymentRequestForMembershipFee(ClubMember clubMember)
    {}

    public void SendCongratulationsForCertification(ClubMember clubMember)
    {}
}