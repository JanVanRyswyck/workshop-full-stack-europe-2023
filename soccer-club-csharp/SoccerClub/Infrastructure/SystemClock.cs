using NodaTime;
using SoccerClub.DomainPorts;

namespace SoccerClub.Infrastructure;

public class SystemClock : Clock
{
    private readonly NodaTime.SystemClock _clock = NodaTime.SystemClock.Instance;

    public LocalDate GetCurrentDate()
    {
        return _clock.GetCurrentInstant().InZone(DateTimeZoneProviders.Tzdb["CET"]).Date;
    }
}