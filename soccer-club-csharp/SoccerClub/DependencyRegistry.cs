using SoccerClub.Domain.ClubMembers;
using SoccerClub.Domain.Teams;
using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.ClubMembers;
using SoccerClub.DomainPorts.Clubs;
using SoccerClub.DomainPorts.Teams;
using SoccerClub.Infrastructure;
using SoccerClub.Infrastructure.ClubMembers;
using SoccerClub.Infrastructure.Clubs;
using SoccerClub.Infrastructure.Teams;

namespace SoccerClub;

public static class DependencyRegistry
{
    public static void RegisterDependencies(this IServiceCollection services)
    {
        RegisterClubDependencies(services);
        RegisterClubMemberDependencies(services);
        RegisterTeamDependencies(services);
        RegisterInfrastructureDependencies(services);
    }

    private static void RegisterClubDependencies(IServiceCollection services)
    {
        services.AddSingleton<ClubRepository, SQLiteClubRepository>();
    }

    private static void RegisterClubMemberDependencies(IServiceCollection services)
    {
        services.AddSingleton<CommandHandler<RegisterClubMemberCommand>, RegisterClubMemberHandler>();
        services.AddSingleton<CommandHandler<AssignCertificationCommand>, AssignCertificationHandler>();
        services.AddSingleton<ICanCreateClubMember, ClubMemberFactory>();
        services.AddSingleton<ClubMemberRepository, SQLiteClubMemberRepository>();
        services.AddSingleton<ClubMemberNotifier, EmailSender>();
    }

    private static void RegisterTeamDependencies(IServiceCollection services)
    {
        services.AddSingleton<CommandHandler<MakeTeamCommand>, MakeTeamHandler>();
        services.AddSingleton<TeamFactory>();
        services.AddSingleton<TeamRepository, SQLiteTeamRepository>();
    }
    
    private static void RegisterInfrastructureDependencies(IServiceCollection services)
    {
        services.AddSingleton<ConfigurationSettings, ApplicationConfigurationSettings>();
        services.AddSingleton<Clock, SystemClock>();
    }
}