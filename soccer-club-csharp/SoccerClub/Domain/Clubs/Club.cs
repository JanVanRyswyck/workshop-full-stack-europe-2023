namespace SoccerClub.Domain.Clubs;

public class Club
{
    public int MasterNumber { get; }
    public string Name { get; }

    public Club(int masterNumber, string name) {
        MasterNumber = masterNumber;
        Name = name;
    }
}