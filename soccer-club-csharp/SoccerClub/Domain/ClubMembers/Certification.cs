using NodaTime;

namespace SoccerClub.Domain.ClubMembers;

public class Certification
{
    public string CertificateCode { get; }
    public LocalDate IssueDate { get; }

    public Certification(string certificateCode, LocalDate issueDate) {

        CertificateCode = certificateCode;
        IssueDate = issueDate;
    }
    
    public override bool Equals(object? obj)
    {
        if(ReferenceEquals(null, obj)) return false;
        if(ReferenceEquals(this, obj)) return true;
        if(obj.GetType() != GetType()) return false;
        return string.Equals(CertificateCode, ((Certification) obj).CertificateCode, StringComparison.OrdinalIgnoreCase);
    }

    public override int GetHashCode()
    {
        return StringComparer.OrdinalIgnoreCase.GetHashCode(CertificateCode);
    }
}