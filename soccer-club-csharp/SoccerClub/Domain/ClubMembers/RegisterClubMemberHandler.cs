using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.ClubMembers;
using SoccerClub.DomainPorts.Clubs;

namespace SoccerClub.Domain.ClubMembers;

public class RegisterClubMemberHandler : CommandHandler<RegisterClubMemberCommand>
{
    public static readonly DomainViolation UnknownClubViolation =
        new DomainViolation("The specified club could not be found.");
    public static readonly DomainViolation ClubMemberAlreadyRegisteredViolation =
        new DomainViolation("The club member has already been registered.");

    private readonly ClubRepository _clubRepository;
    private readonly ClubMemberRepository _clubMemberRepository;
    private readonly ICanCreateClubMember _clubMemberFactory;
    private readonly ClubMemberNotifier _clubMemberNotifier;
    
    public RegisterClubMemberHandler(ClubRepository clubRepository,
        ClubMemberRepository clubMemberRepository,
        ICanCreateClubMember clubMemberFactory,
        ClubMemberNotifier clubMemberNotifier)
    {
        _clubRepository = clubRepository;
        _clubMemberRepository = clubMemberRepository;
        _clubMemberFactory = clubMemberFactory;
        _clubMemberNotifier = clubMemberNotifier;
    }
    
    public Result Handle(RegisterClubMemberCommand command)
    {
        throw new NotImplementedException();
    }
}