using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.ClubMembers;

namespace SoccerClub.Domain.ClubMembers;

public class AssignCertificationHandler : CommandHandler<AssignCertificationCommand>
{
    public static readonly DomainViolation UnknownClubMemberViolation =
        new DomainViolation("The specified club member could not be found.");

    private readonly ClubMemberRepository _clubMemberRepository;
    private readonly ClubMemberNotifier _clubMemberNotifier;
    private readonly Clock _clock;
    
    public AssignCertificationHandler(ClubMemberRepository clubMemberRepository,
        ClubMemberNotifier clubMemberNotifier,
        Clock clock)
    {
        _clubMemberRepository = clubMemberRepository;
        _clubMemberNotifier = clubMemberNotifier;
        _clock = clock;
    }
    
    public Result Handle(AssignCertificationCommand command)
    {
        throw new NotImplementedException();
    }
}