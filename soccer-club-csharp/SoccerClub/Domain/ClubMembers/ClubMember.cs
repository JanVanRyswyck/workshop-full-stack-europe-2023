using NodaTime;
using SoccerClub.Domain.Clubs;
using SoccerClub.DomainPorts;

namespace SoccerClub.Domain.ClubMembers;

public class ClubMember
{
    public readonly static DomainViolation InvalidIssueDateViolation =
        new DomainViolation("The specified issue date cannot be in the future.");
    
    private readonly IList<Certification> _certifications;
    private readonly IList<DomainViolation> _domainViolations;
    
    public Guid Id { get; }
    public int ClubNumber { get; }
    public string FirstName { get; }
    public string LastName { get; }
    public string Email { get; }
    public LocalDate BirthDate { get; }
    public string NationalIdentificationNumber { get; }

    public IEnumerable<Certification> Certifications => _certifications;
    public IEnumerable<DomainViolation> DomainViolations => _domainViolations;

    public ClubMember(Guid id, Club club, string firstName, string lastName, string email, LocalDate birthDate,
                      string nationalIdentificationNumber) 
    {
        Id = id;
        ClubNumber = club.MasterNumber;
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        BirthDate = birthDate;
        NationalIdentificationNumber = nationalIdentificationNumber;

        _certifications = new List<Certification>();
        _domainViolations = new List<DomainViolation>();
    }
    
    public void AssignCertification(string certificateCode, LocalDate issueDate, Clock clock) 
    {
        //
        // Not implemented yet ...
        //
    }
    
    public override bool Equals(object? obj)
    {
        if(ReferenceEquals(null, obj)) return false;
        if(ReferenceEquals(this, obj)) return true;
        if(obj.GetType() != GetType()) return false;

        var other = (ClubMember) obj;
        return Id.Equals(other.Id);
    }

    public override int GetHashCode()
    {
        return Id.GetHashCode();
    }
}