using NodaTime;
using SoccerClub.Domain.Clubs;
using SoccerClub.DomainPorts;

namespace SoccerClub.Domain.ClubMembers;

public interface ICanCreateClubMember
{
    Tuple<ClubMember?, DomainViolation?> CreateClubMember(FactoryData data);
}

public class ClubMemberFactory : ICanCreateClubMember
{
    private const int MinimumAge = 5;
    public static readonly DomainViolation MinimumAgeViolation =
        new DomainViolation($"A club member must be at least {MinimumAge} years old.");

    private readonly Clock _clock;
    
    public ClubMemberFactory(Clock clock) {
        _clock = clock;
    }

    public Tuple<ClubMember?, DomainViolation?> CreateClubMember(FactoryData data)
    {
        throw new NotImplementedException();
    }
}

public class FactoryData {

    public Guid ClubMemberId { get; }
    public Club Club { get; }
    public string FirstName { get; }
    public string LastName { get; }
    public string Email { get; }
    public LocalDate BirthDate { get; }
    public string NationalIdentificationNumber { get; }

    public FactoryData(Guid clubMemberId, Club club, string firstName, string lastName, string email,
                       LocalDate birthDate, string nationalIdentificationNumber) 
    {
        ClubMemberId = clubMemberId;
        Club = club;
        FirstName = firstName;
        LastName = lastName;
        Email = email;
        BirthDate = birthDate;
        NationalIdentificationNumber = nationalIdentificationNumber;
    }
    
    public override bool Equals(object? obj)
    {
        if(ReferenceEquals(null, obj)) return false;
        if(ReferenceEquals(this, obj)) return true;
        if(obj.GetType() != GetType()) return false;

        var other = (FactoryData) obj;
        return ClubMemberId.Equals(other.ClubMemberId) && 
               Club.Equals(other.Club) && 
               FirstName == other.FirstName && 
               LastName == other.LastName && 
               Email == other.Email && 
               BirthDate.Equals(other.BirthDate) && 
               NationalIdentificationNumber == other.NationalIdentificationNumber;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(ClubMemberId, Club, FirstName, LastName, Email, BirthDate, NationalIdentificationNumber);
    }
}