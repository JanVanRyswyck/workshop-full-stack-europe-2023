using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.Clubs;
using SoccerClub.DomainPorts.Teams;

namespace SoccerClub.Domain.Teams;

public class MakeTeamHandler : CommandHandler<MakeTeamCommand>
{
    public static readonly DomainViolation UnknownClubViolation =
        new DomainViolation("The specified club could not be found.");

    private readonly ClubRepository _clubRepository;
    private readonly TeamRepository _teamRepository;
    private readonly TeamFactory _teamFactory;

    public MakeTeamHandler(ClubRepository clubRepository,
        TeamRepository teamRepository,
        TeamFactory teamFactory)
    {
        _clubRepository = clubRepository;
        _teamRepository = teamRepository;
        _teamFactory = teamFactory;
    }


    public Result Handle(MakeTeamCommand command)
    {
        throw new NotImplementedException();
    }
}