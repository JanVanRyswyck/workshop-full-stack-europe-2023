using SoccerClub.Domain.Clubs;

namespace SoccerClub.Domain.Teams;

public class Team
{
    public Guid Id { get; }
    public string Name { get; }
    public Season Season { get; }
    public int ClubNumber { get; }

    public Team(Guid id, Club club, string name, Season season)
    {
        Id = id;
        ClubNumber = club.MasterNumber;
        Name = name;
        Season = season;
    }
}