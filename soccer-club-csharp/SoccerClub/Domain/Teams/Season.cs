namespace SoccerClub.Domain.Teams;

public class Season
{
    public int StartYear { get; }
    public int EndYear { get; }

    public Season(int startYear)
    {
        StartYear = startYear;
        EndYear = startYear + 1;
    }
}