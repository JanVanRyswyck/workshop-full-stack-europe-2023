using NodaTime;
using SoccerClub.Domain.Clubs;
using SoccerClub.DomainPorts;

namespace SoccerClub.Domain.Teams;

public class TeamFactory
{
    public static readonly DomainViolation TeamCompositionPeriodViolation =
        new DomainViolation("A team can only be composed between May 1 and August 31.");
    
    private readonly Clock _clock;

    public TeamFactory(Clock clock)
    {
        _clock = clock;
    }

    public Tuple<Team?, DomainViolation?> CreateTeam(FactoryData data)
    {
        // Tip: The `Clock` can be used to retrieve the current date.
        // The year of the current date can be used to create an instance of the current `Season`.
        throw new NotImplementedException();
    }
}

public class FactoryData
{
    public Guid Id { get; }
    public Club Club { get; }
    public string Name { get; }

    public FactoryData(Guid id, Club club, String name)
    {
        Id = id;
        Club = club;
        Name = name;
    }
}