using NUnit.Framework;
using SoccerClub.Api.Teams;

namespace SoccerClub.Tests.Exercise1_4.Solitary;

public class MakeTeamRequestValidatorTests
{
    [Test]
    public void Validate_ValidRequest_ShouldIndicatePositiveResult()
    {
        var validRequest = new MakeTeamRequest { Name = "U11 A" };
        var isValid = new MakeTeamRequestValidator().Validate(validRequest);
        Assert.That(isValid);    
    }
    
    [TestCase(null)]
    [TestCase("")]
    [TestCase(" ")]
    public void Validate_RequestWithoutName_ShouldIndicateNegativeResult(string invalidName)
    {
        var invalidRequest = new MakeTeamRequest { Name = invalidName };
        var isValid = new MakeTeamRequestValidator().Validate(invalidRequest);
        Assert.That(isValid, Is.False);  
    }
}