using DeepEqual.Syntax;
using NodaTime;
using NUnit.Framework;
using SoccerClub.Domain.Clubs;
using SoccerClub.Domain.Teams;
using SoccerClub.DomainPorts;
using SoccerClub.Tests.Common;

namespace SoccerClub.Tests.Exercise1_4.Solitary;

//
// For this test suite we're going to apply the "Arrange, Act, Assert per test class" approach.
//

// The happy path scenario.
// A new `Team` should be instantiated by the `TeamFactory`. After that, the newly created `Team` should be saved
// in the repository.
public class When_making_a_new_team_for_a_club
{
    [Establish]
    public void Context()
    {
        // Arrange stage
        // Step 1: `create a `Club` using the corresponding test data builder (e.g. `Example.Club()...`).`
        _club = null;

        // Step 2: create an instance of the `MakeTeamHandlerFixture`.
        _fixture = null;
        
        // Step 3: create an instance of the Subject Under Test by using the fixture object.
        // Specify the necessary indirect inputs using the `SutCanQuery` method (e.g. a `Club` object, a
        // `LocalDate` representing the current date). Also make sure to call the `InterceptPersistedTeam`
        // method here before creating the SUT.
        _sut = null;    
    }   
    
    [Because]
    public void Of()
    {
        // Act stage
        // Execute the `Handle` method on the `MakeTeamHandler` instance created in `Context`. You can
        // store the result of the operation in the `_result` member variable.
    }
    
    [Test]
    public void Then_it_should_persist_a_new_team()
    {
        // Assert stage - part 1
        // Observe that a `Team` instance is saved by the `GameRepository`.
        // Use the `PersistedTeam` property of the fixture object to access the captured indirect output (e.g. the
        // persisted `Team`).
        //
        // Tip: Use the `TeamBuilder` (e.g. `Example.Team()...`) to create an expected `Team` instance and use the
        // `ShouldDeepEqual` to verify the saved `Team`.
    }
    
    [Test]
    public void Then_it_should_indicate_a_successful_operation()
    {
        // Assert stage - part 2
        // Observe whether the return value indicates a successful operation.
    }
    
    private Club _club;
    private MakeTeamHandlerFixture _fixture;
    private Result _result;
    private MakeTeamHandler _sut;
}

// Edge case #1
// When the specified club doesn't exist, then the result of the `Handle` method should indicate a failed operation
// by returning a domain violation.
public class When_making_a_new_team_for_an_unknown_club
{
    [Establish]
    public void Context()
    {
        // Arrange stage
        // Create an instance of the Subject Under Test by using the `MakeTeamHandlerFixture`. Specify a null
        // reference to be returned as the indirect input for a `Club`.
        _sut = null;    
    }
    
    [Because]
    public void Of()
    {
        // Act stage
        // Execute the `Handle` method on the `MakeTeamHandler` instance created in `Context`.
    }

    [Test]
    public void Then_it_should_indicate_a_failed_operation()
    {
        // Assert stage
        // Create an instance of the expected result (MakeTeamHandler.UnknownClubViolation), and use
        // `ShouldDeepEqual` to verify the actual result.
    }
    
    private MakeTeamHandler _sut;
    private Result _result;    
}

// Edge case #2
// When a domain violation is issued by the `TeamFactory` while making a new team, then the result of the
// `Handle` method should indicate a failed operation by returning that domain violation.
public class When_making_a_new_team_for_a_club_results_in_a_failure
{
    [Establish]
    public void Context()
    {
        // Arrange stage
        // Create an instance of the Subject Under Test by using the `MakeTeamHandlerFixture`. Specify the necessary
        // indirect inputs using the `SutCanQuery` method (e.g. a `Club` object, a `LocalDate` representing the
        // current date).
        _sut = null;
    }

    [Because]
    public void Of()
    {
        // Act stage
        // Execute the `Handle` method on the `MakeTeamHandler` instance created in `Context`.
    }
    
    [Test]
    public void Then_it_should_indicate_a_failed_operation()
    {
        // Assert stage
        // Create an instance of the expected result (TeamFactory.TeamCompositionPeriodViolation), and use
        // `ShouldDeepEqual` to verify the actual result.
    }
    
    private MakeTeamHandler _sut;
    private Result _result;
}