using DeepEqual.Syntax;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;
using SoccerClub.Api.Teams;
using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.Teams;
using SoccerClub.Tests.Common;

namespace SoccerClub.Tests.Exercise1_4.Solitary;

public class When_making_a_new_team
{
    [Establish]
    public void Context()
    {
        _fixture = new MakeTeamResourceFixture();
        _sut = _fixture
            .SutCanQuery(TeamId)
            .SutCanQuery(Result.Success())
            .BuildSut();       
    }
    
    [Because]
    public void Of()
    {
        var request = new MakeTeamRequest { Name = "U13 B" };
        _response = _sut.MakeTeam(4873, request);
    }
    
    [Test]
    public void Then_it_should_dispatch_a_command_to_the_domain() 
    {
        var expectedCommand = new MakeTeamCommand(4873, TeamId,"U13 B");
        _fixture.DispatchedCommand.ShouldDeepEqual(expectedCommand);
    }
    
    [Test]
    public void Then_it_should_respond_with_status_code_OK()
    {
        Assert.That(_response, Is.TypeOf<OkObjectResult>());
    }
    
    [Test]
    public void Then_it_should_respond_with_the_new_team_identifier()
    {
        var expectedResponse = new MakeTeamResponse { TeamId = TeamId };
        ((ObjectResult) _response).Value.ShouldDeepEqual(expectedResponse);
    }
    
    private ActionResult _response;
    private MakeTeamResource _sut;
    private MakeTeamResourceFixture _fixture;

    private static readonly Guid TeamId = Guid.Parse("06f1db66-163a-11ec-94ab-acde48001122");
}

public class When_making_a_new_team_using_an_invalid_request
{
    [Establish]
    public void Context()
    {
        _sut = new MakeTeamResourceFixture()
            .BuildSut();    
    }   
    
    [Because]
    public void Of()
    {
        var invalidRequest = new MakeTeamRequest { Name = null };
        _response = _sut.MakeTeam(4873, invalidRequest);    
    }
    
    [Test]
    public void Then_it_should_respond_with_status_code_bad_request()
    {
        Assert.That(_response, Is.TypeOf<BadRequestResult>());
    }
    
    private ActionResult _response;
    private MakeTeamResource _sut;
}

public class When_making_a_new_team_results_in_a_domain_violation
{
    [Establish]
    public void Context()
    {
        _sut = new MakeTeamResourceFixture()
            .SutCanQuery(Guid.Parse("06f1db66-163a-11ec-94ab-acde48001122"))
            .SutCanQuery(Result.Failure(new DomainViolation("Request does not meet domain criteria.")))
            .BuildSut();        
    }
    
    [Because]
    public void Of()
    {
        var request = new MakeTeamRequest { Name = "U13" };
        _response = _sut.MakeTeam(4873, request);
    }
    
    [Test]
    public void Then_it_should_respond_with_status_code_bad_request()
    {
        Assert.That(_response, Is.TypeOf<BadRequestResult>());
    }
    
    private ActionResult _response;
    private MakeTeamResource _sut;
}

public class MakeTeamResourceFixture {

    private readonly CommandHandler<MakeTeamCommand> _makeTeamHandler;
    private readonly TeamRepository _teamRepository;

    public MakeTeamCommand DispatchedCommand { get; private set; }

    public MakeTeamResourceFixture() 
    {
        _makeTeamHandler = Substitute.For<CommandHandler<MakeTeamCommand>>();
        _teamRepository = Substitute.For<TeamRepository>();
    }

    public MakeTeamResourceFixture SutCanQuery(Guid teamId) 
    {
        _teamRepository.GetNextIdentity().Returns(teamId);
        return this;
    }

    public MakeTeamResourceFixture SutCanQuery(Result result) 
    {
        _makeTeamHandler.Handle(Arg.Do<MakeTeamCommand>(captured => DispatchedCommand = captured))
            .Returns(result);
        return this;
    }

    public MakeTeamResource BuildSut() 
    {
        return new MakeTeamResource(_teamRepository, _makeTeamHandler);
    }
}