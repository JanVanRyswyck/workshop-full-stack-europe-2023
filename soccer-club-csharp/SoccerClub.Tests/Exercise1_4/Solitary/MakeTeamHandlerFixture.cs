using NodaTime;
using SoccerClub.Domain.Clubs;
using SoccerClub.Domain.Teams;

namespace SoccerClub.Tests.Exercise1_4.Solitary;

public class MakeTeamHandlerFixture
{
    // Step 1: uncomment these private fields / public property
    // private readonly Clock _clock;
    // private readonly ClubRepository _clubRepository;
    // private readonly TeamRepository _teamRepository;

    // public Team PersistedTeam { get; private set; }
    
    public MakeTeamHandlerFixture()
    {
        // Step 2: initialise the private fields with test doubles (e.g. `Substitute.For<...>()`).
    }
    
    public MakeTeamHandlerFixture SutCanQuery(Club club) 
    {
        // Step 3: let the `ClubRepository` return the specified `Club` instance.
        // Tip: You can use `Arg.Any<int>()` to allow for flexible argument matching.
        return this;
    }

    public MakeTeamHandlerFixture SutCanQuery(LocalDate currentDate) 
    {
        // Step 4: let the `Clock` return the specified current date.
        return this;
    }

    public MakeTeamHandlerFixture InterceptPersistedTeam()
    {
        // Step 6: the purpose of this method is to capture the instance of a `Team` when it's provided to the
        // `Save` method of the `TeamRepository`. This captured `Team` object should be assigned to the `PersistedTeam`
        // property.
        //
        // Tip: You can make use of an argument captor (e.g. `Arg.Do<Team>(captured => PersistedTeam = captured)`).
        return this;
    }

    public MakeTeamHandler BuildSut() 
    {
        // Step 6: build an instance of the `MakeTeamHandler` using the private fields.
        // Tip: Use a concrete instance of the `TeamFactory` instead of a test double.
        return null;
    }
}