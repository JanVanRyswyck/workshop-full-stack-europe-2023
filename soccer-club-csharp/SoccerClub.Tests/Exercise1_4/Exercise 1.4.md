# Exercise 1.4

![](Exercise_1.4.png)

## 1. Solitary tests

## 1.1 MakeTeamHandlerFixture

Open the file `MakeTeamHandlerFixture.java`. Follow the steps as outlined in the comments.

## 1.2 MakeTeamHandler

1. Open the files `MakeTeamHandler.java` and `MakeTeamHandlerTests.java`.
2. Complete the implementation of the provided test scenarios using a Test-First approach. Remember to only implement a
   single test case at a time. Try to use the `MakeTeamHandlerFixture` as much as possible instead of directly interacting
   with test doubles in tests. Also make use of the `ClubBuilder`, `SeasonBuilder` and `TeamBuilder` whenever possible.

## 2. Sociable tests

Open the file `MakeTeamTests.java`. There's a single Sociable test that calls the API endpoint for making a new team.
The test verifies the status code of the HTTP response. Go ahead and execute this test. At this point, the test should 
be green as you implemented all the missing pieces in the previous steps.

There's nothing that should be done for this Sociable test as it has already been completely implemented. You can proceed
to the next step.

## 3. Open questions

* Did you experience the pros and cons of using a Fixture Object compared to using test doubles directly?
* Would you consider introducing the Fixture Object pattern in your own codebase? 
* Did you notice any synergies between the Fixture Object pattern and the Test Data Builder pattern?
