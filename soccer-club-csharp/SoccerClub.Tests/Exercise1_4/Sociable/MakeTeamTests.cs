using System.Net;
using System.Net.Http.Json;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using NodaTime;
using NUnit.Framework;
using SoccerClub.Api.Teams;
using SoccerClub.Domain.Clubs;
using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.Clubs;
using SoccerClub.Tests.Common.Database;
using SoccerClub.Tests.Common.Fakes;
using SoccerClub.Tests.Common.Host;

namespace SoccerClub.Tests.Exercise1_4.Sociable;

public class MakeTeamTests
{
    private const string UuidPattern = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";
    
    private SoccerClubApplication _testApplication;
    private SoccerClubDatabase _soccerClubDatabase;
    private ClubRepository _clubRepository;
    private HttpClient _testHttpClient;
    
    [SetUp]
    public void SetUp()
    {
        _testApplication = new SoccerClubApplication()
            .WithTestDependencies(services =>
            {
                services.RemoveAll<Clock>();
                services.AddSingleton<Clock>(new FakeClock(new LocalDate(2021, 06, 15)));
            });
        
        _soccerClubDatabase = _testApplication.GetRequiredService<SoccerClubDatabase>();
        _soccerClubDatabase.Initialise()
            .EnforceForeignKeys();
        
        _clubRepository = _testApplication.GetRequiredService<ClubRepository>();
        _testHttpClient = _testApplication.CreateClient();
    }    
    
    [TearDown]
    public void TearDown()
    {
        _soccerClubDatabase.Cleanup();
        _testApplication.Dispose();
    }
    
    [Test]
    public async Task Should_receive_an_OK_response_together_with_the_identifier_of_the_newly_made_team()
    {
        _clubRepository.Save(new Club(4873, "Bayern München"));
        
        var request = new MakeTeamRequest { Name = "Team A" };
        var response = await _testHttpClient.PostAsJsonAsync("/api/clubs/4873/teams", request);
        
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        
        var body = response.Content.ReadFromJsonAsync<MakeTeamResponse>().Result;
        Assert.That(body, Is.Not.Null);
        Assert.That(body.TeamId, Is.Not.Empty);
        Assert.That(body.TeamId.ToString(), Does.Match(UuidPattern));
    }
}