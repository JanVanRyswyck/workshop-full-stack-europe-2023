using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using SoccerClub.Infrastructure;
using SoccerClub.Tests.Common.Database;

namespace SoccerClub.Tests.Common.Host;

internal class SoccerClubApplication : WebApplicationFactory<Program>
{
    private Action<IServiceCollection> _configureTestDependencies = (_) => {};
    
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        base.ConfigureWebHost(builder);

        builder.ConfigureServices(services =>
        {
            services.RemoveAll<ConfigurationSettings>();
            services.AddSingleton<ConfigurationSettings, TestConfigurationSettings>();

            services.AddSingleton<SoccerClubDatabase>();
            _configureTestDependencies(services);
        });
    }

    public SoccerClubApplication WithTestDependencies(Action<IServiceCollection> configureTestDependencies)
    {
        _configureTestDependencies = configureTestDependencies;
        return this;
    }
    
    public T GetRequiredService<T>() where T : notnull
    {
        return Services.GetRequiredService<T>();
    }
}