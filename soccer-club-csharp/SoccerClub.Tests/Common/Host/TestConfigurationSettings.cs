using SoccerClub.Infrastructure;
using SoccerClub.Tests.Common.Database;

namespace SoccerClub.Tests.Common.Host;

public class TestConfigurationSettings : ConfigurationSettings
{
    private readonly string _connectionString = SQLiteConnectionUrl.Build();

    public string GetConnectionString()
    {
        return _connectionString;
    }
}