using NUnit.Framework;

namespace SoccerClub.Tests.Common;

public class SpecificationAttribute : TestFixtureAttribute
{}

public class EstablishAttribute : OneTimeSetUpAttribute
{}

public class BecauseAttribute : OneTimeSetUpAttribute
{}

public class ObservationAttribute : TestAttribute
{}