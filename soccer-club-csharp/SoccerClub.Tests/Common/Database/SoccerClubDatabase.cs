using System.Reflection;
using SoccerClub.Infrastructure;

namespace SoccerClub.Tests.Common.Database;

public class SoccerClubDatabase
{
    private readonly SQLiteDatabasePopulator _databasePopulator;

    public SoccerClubDatabase(ConfigurationSettings configurationSettings)
    {
        _databasePopulator = new SQLiteDatabasePopulator(configurationSettings.GetConnectionString());
    }
    
    public SoccerClubDatabase Initialise()
    {
        var soccerClubSchema = ReadSqlResource("SoccerClub.Database.InitialiseSchema.sql");
        _databasePopulator.Execute(soccerClubSchema);
        return this;
    }

    public void Cleanup()
    {
        var cleanupSchema = ReadSqlResource("SoccerClub.Database.CleanupSchema.sql");
        _databasePopulator.Execute(cleanupSchema);
    }

    public void EnforceForeignKeys()
    {
        _databasePopulator.Execute("PRAGMA foreign_keys = ON;");
    }
    
    private static string ReadSqlResource(string sqlFileName)
    {
        var mainAssembly = Assembly.GetAssembly(typeof(Program));
        if(mainAssembly == null)
            throw new InvalidOperationException("Unable to find the SoccerClub assembly.");
        
        using var resourceStream = mainAssembly.GetManifestResourceStream(sqlFileName);
        if(resourceStream == null)
            throw new FileNotFoundException($"Unable to read embedded SQL file with name '{sqlFileName}'.");
        
        using var streamReader = new StreamReader(resourceStream);
        return streamReader.ReadToEnd();
    }
}