namespace SoccerClub.Tests.Common.Database;

public static class SQLiteConnectionUrl
{
    public static string Build()
    {
        var sqliteDatabaseFile = Path.Combine(Path.GetTempPath(), "SoccerClubTest.db");
        return $"Data Source={sqliteDatabaseFile}";
    }
}