using Microsoft.Data.Sqlite;

namespace SoccerClub.Tests.Common.Database;

public class SQLiteDatabasePopulator
{
    private readonly string _connectionString;

    public SQLiteDatabasePopulator(string connectionString)
    {
        _connectionString = connectionString;
    }

    public void Execute(string sqlScript)
    {
        using var connection = new SqliteConnection(_connectionString);
        using var command = new SqliteCommand(sqlScript, connection);
        
        connection.Open();
        command.ExecuteNonQuery();
    }
}