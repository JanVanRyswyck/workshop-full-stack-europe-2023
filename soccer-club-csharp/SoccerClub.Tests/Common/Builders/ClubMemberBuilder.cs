using NodaTime;
using SoccerClub.Domain.ClubMembers;
using SoccerClub.Domain.Clubs;

namespace SoccerClub.Tests.Common.Builders;

public class ClubMemberBuilder
{
    private Guid _id = Guid.Parse("d8811d8f-ae7c-4ff9-a6c8-3815957f07ea");
    private Club _club = Example.Club();
    private string _firstName = "Robert";
    private string _lastName = "Lewandowski";
    private string _email = "robert@lewandowski.pl";
    private LocalDate _birthDate = new(1988, 08, 21);
    private string _nationalIdentificationNumber = "88082138475";

    public ClubMemberBuilder WithId(Guid id) 
    {
        _id = id;
        return this;
    }

    public ClubMemberBuilder WithClub(Club club) 
    {
        _club = club;
        return this;
    }

    public ClubMemberBuilder WithFirstName(string firstName) 
    {
        _firstName = firstName;
        return this;
    }

    public ClubMemberBuilder WithLastName(string lastName) 
    {
        _lastName = lastName;
        return this;
    }

    public ClubMemberBuilder WithEmail(string email) 
    {
        _email = email;
        return this;
    }

    public ClubMemberBuilder WithBirthDate(LocalDate birthDate) 
    {
        _birthDate = birthDate;
        return this;
    }

    public ClubMemberBuilder WithNationalIdentificationNumber(string nationalIdentificationNumber) 
    {
        _nationalIdentificationNumber = nationalIdentificationNumber;
        return this;
    }

    public ClubMember Build() 
    {
        return new ClubMember(_id, _club, _firstName, _lastName, _email, _birthDate, _nationalIdentificationNumber);
    }
    
    public static implicit operator ClubMember(ClubMemberBuilder builder)
    {
        return builder.Build();
    }
}