using SoccerClub.Domain.Clubs;

namespace SoccerClub.Tests.Common.Builders;

public class ClubBuilder
{
    private int _masterNumber;
    private string _name;

    public ClubBuilder()
    {
        // Step 1: provide default values (test data) for the private fields.
    }
    
    public ClubBuilder WithMasterNumber(int masterNumber) 
    {
        // Step 2: override the default `masterNumber` with the specified value.
        throw new NotImplementedException();
    }

    public ClubBuilder WithName(string name) 
    {
        // Step 3: override the default `name` with the specified value.
        throw new NotImplementedException();
    }

    public Club Build() 
    {
        // Step 4: Build an instance of a `Club` using the private fields.
        throw new NotImplementedException();
    }
    
    public static implicit operator Club(ClubBuilder builder)
    {
        return builder.Build();
    }
}