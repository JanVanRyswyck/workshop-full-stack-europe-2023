using SoccerClub.Domain.Teams;

namespace SoccerClub.Tests.Common.Builders;

public class SeasonBuilder
{
    private int _startYear;

    public SeasonBuilder()
    {
        // Step 1: provide default values (test data) for the private field.
    }
    
    public SeasonBuilder WithStartYear(int startYear) 
    {
        // Step 2: override the default `startYear` with the specified value.
        throw new NotImplementedException();
    }

    public Season Build() 
    {
        // Step 3: build an instance of a `Season` using the private field.
        throw new NotImplementedException();
    }
    
    public static implicit operator Season(SeasonBuilder builder)
    {
        return builder.Build();
    }
    
    // Step 4: Go to the class `Example`, and add a factory method for creating an instance of a `SeasonBuilder`.
}