using SoccerClub.Domain.Clubs;
using SoccerClub.Domain.Teams;

namespace SoccerClub.Tests.Common.Builders;

public class TeamBuilder
{
    private Guid _id;
    private string _name;
    private SeasonBuilder _season;
    private Club _club;

    public TeamBuilder()
    {
        // Step 1: provide default values (test data) for the private field.
        // Use other test data builders for complex types (e.g. `Example.club()`).
    }
    
    public TeamBuilder WithId(Guid id) 
    {
        // Step 2: override the default `id` with the specified value.
        throw new NotImplementedException();
    }

    public TeamBuilder WithName(string name) 
    {
        // Step 3: override the default `name` with the specified value.
        throw new NotImplementedException();
    }

    public TeamBuilder WithSeason(Action<SeasonBuilder> build) 
    {
        // Step 4: by accepting a `Consumer<>`, we improve upon the readability of the test data builder DSL.
        // e.g. `.withSeason(season -> season.withStartYear(2021))`
        // A `Consumer<>` can be invoked by calling its `accept` method, specifying the necessary parameter.
        throw new NotImplementedException();
    }

    public TeamBuilder WithClub(Club club)
    {
        // Step 5: sometimes it can also come in handy to just accept an instance of another complex object.
        // In that case we just assign the specified instance to the corresponding field.
        throw new NotImplementedException();
    }

    public Team Build()
    {
        // Step 6: build an instance of a `Team` using the private fields.
        throw new NotImplementedException();
    }
    
    public static implicit operator Team(TeamBuilder builder)
    {
        return builder.Build();
    }
    
    // Step 7: Go to the class `Example`, and add a factory method for creating an instance of a `TeamBuilder`.
}