using NodaTime;
using SoccerClub.DomainPorts;

namespace SoccerClub.Tests.Common.Fakes;

public class FakeClock : Clock
{
    private readonly LocalDate _currentDate;

    public FakeClock(LocalDate currentDate)
    {
        _currentDate = currentDate;
    }

    public LocalDate GetCurrentDate()
    {
        return _currentDate;
    }
}