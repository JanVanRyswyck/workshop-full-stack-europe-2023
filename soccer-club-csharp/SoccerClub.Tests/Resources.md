# Resources

* [NUnit documentation](https://docs.nunit.org/articles/nunit/intro.html)
* [NSubstitute documentation](https://nsubstitute.github.io/help.html)
* [DeepEqual documentation](https://github.com/jamesfoster/DeepEqual)