# Exercise 1.3 - Part 2

![](Exercise_1.3_Part_2.png)

## 1. TeamFactory

1. Open the files `TeamFactory.java` and `TeamFactoryTests.java`.
2. Complete the implementation of the provided test scenarios using a Test-First approach. Remember to only implement a
   single test case at a time. Try to use the `ClubBuilder`, `SeasonBuilder` and `TeamBuilder` as much as possible in
   the test code. 
   
## 2. Open questions

* What do you think about the implementation and usage the Test Data Builder pattern?
* What are your thoughts about “Object State Verification” compared to “Procedural State Verification”. 
* In which cases would you use one approach over the other? Why?
