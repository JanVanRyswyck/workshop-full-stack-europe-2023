using SoccerClub.Domain.Clubs;
using SoccerClub.Domain.Teams;
using SoccerClub.DomainPorts;
using SoccerClub.Tests.Common;

namespace SoccerClub.Tests.Exercise1_3.Solitary;

// The happy path scenario.
// Here we're going to observe whether a `Team` is correctly created by the `TeamFactory`.
public class When_creating_a_new_team
{
    [Establish]
    public void Context()
    {
        // Arrange stage
        // Create an instance of a `TeamFactory` (SUT) and everything that is necessary for executing the scenario.
    }
    
    [Because]
    public void Of()
    {
        // Act stage
        // Execute the `CreateTeam` method on the `TeamFactory` instance created in `Context`.
    }
    
    [Observation]
    public void Then_a_new_team_should_be_created()
    {
        // Assert stage
        // The `CreateTeam` method should return a tuple, with a newly created `Team` on the left and a null
        // reference on the right. Instead of writing an assert statement for each individual property, use the
        // `ShouldDeepEqual` to verify the result.
        // e.g. `_result.ShouldDeepEqual(expectedResult)`
        //
        // Tip: You can use the test data builders for initializing the expected `Team` instance.
    }
    
    private Club _club;
    private Tuple<Team, DomainViolation> _creationResult;
    private TeamFactory _sut;
}

// Edge case #1
// The soccer association imposes on clubs that a team can only be composed after the end of one season and the
// beginning of a new season (between May 1 and August 31). When the current date is before the allowed composition
// period, then the `CreateTeam` method should indicate a failed operation by returning that domain violation.
public class When_creating_a_new_team_before_the_start_of_the_authorised_composition_period
{
    [Establish]
    public void Context()
    {
        // Arrange stage
        // Create an instance of a `TeamFactory` (SUT) and everything that is necessary for executing the scenario.
        // The `GetCurrentDate` method of the test double for `Clock` should return April 30.
    }
    
    [Because]
    public void Of()
    {
        // Act stage
        // Execute the `CreateTeam` method on the `TeamFactory` instance created in `Context`.
    }
    
    [Observation]
    public void Then_it_should_indicate_a_failure()
    {
        // Assert stage
        // The `CreateTeam` method should return a tuple, with a null reference on the left and a domain violation
        // on the right. Instead of writing an assert statement for each individual property, use the
        // `ShouldDeepEqual` to verify the result.
        // e.g. `_result.ShouldDeepEqual(expectedResult)`
        //
        // Tip: The `TeamCompositionPeriodViolation` static field can be used as the domain violation message.
    }
    
    private Tuple<Team, DomainViolation> _creationResult;
    private TeamFactory _sut;
}

// Edge case #2
// Here we're going to verify the same rule as described for the previous scenario. This time, when the current date
// is after the allowed composition period of a team, then the `createTeam` method should indicate a failed operation
// by returning that domain violation.
public class When_creating_a_new_team_after_the_end_of_the_authorised_composition_period
{
    [Establish]
    public void Context()
    {
        // Arrange stage
        // Create an instance of a `TeamFactory` (SUT) and everything that is necessary for executing the scenario.
        // The `GetCurrentDate` method of the test double for `Clock` should return September 1.
    }

    [Because]
    public void Of()
    {
        // Act stage
        // Execute the `CreateTeam` method on the `TeamFactory` instance created in `Context`.
    }

    [Observation]
    public void Then_it_should_indicate_a_failure()
    {
        // Assert stage
        // The `CreateTeam` method should return a tuple, with a null reference on the left and a domain violation
        // on the right. Instead of writing an assert statement for each individual property, use the
        // `ShouldDeepEqual` to verify the result.
        // e.g. `_result.ShouldDeepEqual(expectedResult)`
        //
        // Tip: The `TeamCompositionPeriodViolation` static field can be used as the domain violation message.
    }

    private Tuple<Team, DomainViolation> _creationResult;
    private TeamFactory _sut;
}