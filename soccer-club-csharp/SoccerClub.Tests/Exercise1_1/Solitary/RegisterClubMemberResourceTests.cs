using DeepEqual.Syntax;
using Microsoft.AspNetCore.Mvc;
using NodaTime;
using NSubstitute;
using NUnit.Framework;
using SoccerClub.Api.ClubMembers;
using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.ClubMembers;

namespace SoccerClub.Tests.Exercise1_1.Solitary;

public class RegisterClubMemberResourceTests
{
    private ClubMemberRepository _clubMemberRepository;
    private CommandHandler<RegisterClubMemberCommand> _registerClubMemberHandler;
    private RegisterClubMemberResource _sut;
    
    [SetUp]
    public void SetUp()
    {
        _registerClubMemberHandler = Substitute.For<CommandHandler<RegisterClubMemberCommand>>();
        _clubMemberRepository = Substitute.For<ClubMemberRepository>();

        _sut = new RegisterClubMemberResource(_clubMemberRepository, _registerClubMemberHandler);
    }
    
    [Test]
    public void RegisterClubMember_ValidRequest_ShouldDispatchCommandToDomain() 
    {
        var clubMemberId = Guid.Parse("c14cdb79-39ef-4248-95d4-209eb6d9999c");
        _clubMemberRepository.GetNextIdentity().Returns(clubMemberId);
        
        RegisterClubMemberCommand dispatchedCommand = null;
        _registerClubMemberHandler.Handle(Arg.Do<RegisterClubMemberCommand>(captured => dispatchedCommand = captured))
            .Returns(Result.Success());
        
        var request = new RegisterClubMemberRequest
        {
            FirstName = "Xavi", 
            LastName = "Hernández", 
            Email = "xavi@hernandez.es",
            BirthDate = new DateTime(1980, 01, 25), 
            NationalIdentificationNumber = "80012546532"
        };
        _sut.RegisterClubMember(5342, request);

        var expectedCommand = new RegisterClubMemberCommand(5342, clubMemberId, "Xavi", "Hernández",
            "xavi@hernandez.es", new LocalDate(1980, 01, 25), "80012546532");

        dispatchedCommand.ShouldDeepEqual(expectedCommand);
    }
    
    [Test]
    public void RegisterClubMember_ValidRequest_ShouldRespondWithStatusCodeOK() 
    {
        var clubMemberId = Guid.Parse("c14cdb79-39ef-4248-95d4-209eb6d9999c");
        _clubMemberRepository.GetNextIdentity().Returns(clubMemberId);
        _registerClubMemberHandler.Handle(Arg.Any<RegisterClubMemberCommand>()).Returns(Result.Success());
        
        var request = new RegisterClubMemberRequest
        {
            FirstName = "Xavi", 
            LastName = "Hernández", 
            Email = "xavi@hernandez.es",
            BirthDate = new DateTime(1980, 01, 25), 
            NationalIdentificationNumber = "80012546532"
        };

        var response = _sut.RegisterClubMember(5342, request);
        Assert.That(response, Is.TypeOf<OkObjectResult>());
    }
    
    [Test]
    public void RegisterClubMember_ValidRequest_ShouldRespondWithNewClubMemberIdentifier() 
    {
        var clubMemberId = Guid.Parse("c14cdb79-39ef-4248-95d4-209eb6d9999c");
        _clubMemberRepository.GetNextIdentity().Returns(clubMemberId);
        _registerClubMemberHandler.Handle(Arg.Any<RegisterClubMemberCommand>()).Returns(Result.Success());
        
        var request = new RegisterClubMemberRequest
        {
            FirstName = "Xavi", 
            LastName = "Hernández", 
            Email = "xavi@hernandez.es",
            BirthDate = new DateTime(1980, 01, 25), 
            NationalIdentificationNumber = "80012546532"
        };

        var response = _sut.RegisterClubMember(5342, request);

        var expectedResponse = new RegisterClubMemberResponse { ClubMemberId = clubMemberId };
        ((ObjectResult)response).Value.ShouldDeepEqual(expectedResponse);
    }
    
    [Test]
    public void RegisterClubMember_InvalidRequest_ShouldRespondWithStatusCodeBadRequest() 
    {
        var invalidRequest = new RegisterClubMemberRequest();
        var response = _sut.RegisterClubMember(5342, invalidRequest);
        Assert.That(response, Is.TypeOf<BadRequestResult>());
    } 
    
    [Test]
    public void RegisterClubMember_RequestDoesNotMeetDomainCriteria_ShouldRespondWithStatusCodeBadRequest() 
    {
        _clubMemberRepository.GetNextIdentity().Returns(Guid.NewGuid());
        _registerClubMemberHandler.Handle(Arg.Any<RegisterClubMemberCommand>())
            .Returns(Result.Failure(new DomainViolation("Request does not meet domain criteria.")));

        var requestToBeRejected = new RegisterClubMemberRequest
        {
            FirstName = "Arwen", 
            LastName = "Too Young", 
            Email = "arwen@tooyoung.com",
            BirthDate = new DateTime(1980, 01, 25), 
            NationalIdentificationNumber = "19081283364"
        };
        
        var response = _sut.RegisterClubMember(5342, requestToBeRejected);
        Assert.That(response, Is.TypeOf<BadRequestResult>());
    } 
}