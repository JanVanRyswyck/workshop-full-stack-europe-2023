using NSubstitute;
using NUnit.Framework;
using SoccerClub.Domain.ClubMembers;
using SoccerClub.DomainPorts;

namespace SoccerClub.Tests.Exercise1_1.Solitary;

public class ClubMemberFactoryTests
{
    private Clock _clock;
    private ClubMemberFactory _sut;

    [SetUp]
    public void SetUp()
    {
        _clock = Substitute.For<Clock>();
        _sut = new ClubMemberFactory(_clock);
    }
    
    [Test]
    public void CreateClubMember_ShouldCreateNewClubMember()
    {
        // The happy path scenario.
        // The `CreateClubMember` method should return a tuple, with a newly created `ClubMember` on the left and
        // a null reference on the right.
    }
    
    [Test]
    public void createClubMember_youngerThanMinimumAge_shouldIndicateFailure() 
    {
        // The soccer association imposes a general rule on clubs: a member should not be younger than five years old.
        // A child of only four years old or younger is not allowed to play soccer in any competition.
        // As a result, when the specified birthdate indicates that the person in question is not at least five years
        // old, the `CreateClubMember` method should return a tuple with a null reference on the left, and a domain
        // violation on the right.
        //
        // Tip: The `Clock` can be used to retrieve the current date.
        // Tip: The `MinimumAgeViolation` static field can be used as the domain violation message.
    }
}