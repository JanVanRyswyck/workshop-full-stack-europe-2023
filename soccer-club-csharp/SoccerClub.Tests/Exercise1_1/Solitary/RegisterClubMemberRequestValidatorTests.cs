using NUnit.Framework;
using SoccerClub.Api.ClubMembers;

namespace SoccerClub.Tests.Exercise1_1.Solitary;

[TestFixture]
public class RegisterClubMemberRequestValidatorTests
{
    [Test]
    public void Validate_ValidRequest_ShouldIndicatePositiveResult()
    {
        var validRequest = new RegisterClubMemberRequest
        {
            FirstName = "Raheem", 
            LastName = "Sterling", 
            Email = "raheem@sterling.co.uk",
            BirthDate = new DateTime(1994, 12, 08), 
            NationalIdentificationNumber = "94120874634" 
        };

        var isValid = new RegisterClubMemberRequestValidator().Validate(validRequest);
        Assert.That(isValid);
    }
    
    [TestCase(null)]
    [TestCase("")]
    [TestCase(" ")]
    public void Validate_RequestWithoutFirstName_ShouldIndicateNegativeResult(string invalidFirstName) 
    {
        var invalidRequest = new RegisterClubMemberRequest
        {
            FirstName = invalidFirstName, 
            LastName = "Sterling", 
            Email = "raheem@sterling.co.uk",
            BirthDate = new DateTime(1994, 12, 08), 
            NationalIdentificationNumber = "94120874634"
        };

        var isValid = new RegisterClubMemberRequestValidator().Validate(invalidRequest);
        Assert.That(isValid, Is.False);
    }
    
    [TestCase(null)]
    [TestCase("")]
    [TestCase(" ")]
    public void Validate_RequestWithoutLastName_ShouldIndicateNegativeResult(string invalidLastName) 
    {
        var invalidRequest = new RegisterClubMemberRequest
        {
            FirstName = "Raheem", 
            LastName = invalidLastName, 
            Email = "raheem@sterling.co.uk",
            BirthDate = new DateTime(1994, 12, 08), 
            NationalIdentificationNumber = "94120874634"
        };
        
        var isValid = new RegisterClubMemberRequestValidator().Validate(invalidRequest);
        Assert.That(isValid, Is.False);
    }
    
    [TestCase(null)]
    [TestCase("")]
    [TestCase(" ")]
    public void Validate_RequestWithoutEmail_ShouldIndicateNegativeResult(string invalidEmail) 
    {
        var invalidRequest = new RegisterClubMemberRequest
        {
            FirstName = "Raheem", 
            LastName = "Sterling", 
            Email = invalidEmail,
            BirthDate = new DateTime(1994, 12, 08), 
            NationalIdentificationNumber = "94120874634"
        };
        
        var isValid = new RegisterClubMemberRequestValidator().Validate(invalidRequest);
        Assert.That(isValid, Is.False);
    }
    
    [Test]
    public void Validate_RequestWithoutBirthDate_ShouldIndicateNegativeResult() 
    {
        var invalidRequest = new RegisterClubMemberRequest
        {
            FirstName = "Raheem", 
            LastName = "Sterling", 
            Email = "raheem@sterling.co.uk",
            BirthDate = null, 
            NationalIdentificationNumber = "94120874634"
        };
        
        var isValid = new RegisterClubMemberRequestValidator().Validate(invalidRequest);
        Assert.That(isValid, Is.False);
    }
    
    [TestCase(null)]
    [TestCase("")]
    [TestCase(" ")]
    public void Validate_RequestWithoutNationalIdentificationNumber_ShouldIndicateNegativeResult(string invalidNationalIdentificationNumber) 
    {
        var invalidRequest = new RegisterClubMemberRequest
        {
            FirstName = "Raheem", 
            LastName = "Sterling", 
            Email = "raheem@sterling.co.uk",
            BirthDate = new DateTime(1994, 12, 08),
            NationalIdentificationNumber = invalidNationalIdentificationNumber
        };
        
        var isValid = new RegisterClubMemberRequestValidator().Validate(invalidRequest);
        Assert.That(isValid, Is.False);
    }
    
    [TestCase("4120874634")]    // Less than 11 digits
    [TestCase("941208746345")]  // More than 11 digits
    [TestCase("94120E74634")]   // Only digits are allowed
    [TestCase("95120874634")]   // Year for date of birth does not match
    [TestCase("94130874634")]   // Month for date of birth does not match
    [TestCase("94120974634")]   // Day for date of birth does not match
    public void Validate_RequestWithInvalidNationalIdentificationNumber_ShouldIndicateNegativeResult(string invalidNationalIdentificationNumber)
    {
        var invalidRequest = new RegisterClubMemberRequest
        {
            FirstName = "Raheem", 
            LastName = "Sterling", 
            Email = "raheem@sterling.co.uk",
            BirthDate = new DateTime(1994, 12, 08),
            NationalIdentificationNumber = invalidNationalIdentificationNumber
        };

        var isValid = new RegisterClubMemberRequestValidator().Validate(invalidRequest);
        Assert.That(isValid, Is.False);
    }
}