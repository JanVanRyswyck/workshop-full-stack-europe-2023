using NodaTime;
using NSubstitute;
using NUnit.Framework;
using SoccerClub.Domain.ClubMembers;
using SoccerClub.Domain.Clubs;
using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.ClubMembers;
using SoccerClub.DomainPorts.Clubs;

namespace SoccerClub.Tests.Exercise1_1.Solitary;

public class RegisterClubMemberHandlerTests
{
    private ClubRepository _clubRepository;
    private ClubMemberRepository _clubMemberRepository;
    private ClubMemberNotifier _clubMemberNotifier;
    private ICanCreateClubMember _clubMemberFactory;
    private RegisterClubMemberHandler _sut;
    
    [SetUp]
    public void SetUp()
    {
        _clubRepository = Substitute.For<ClubRepository>();
        _clubMemberRepository = Substitute.For<ClubMemberRepository>();
        _clubMemberNotifier = Substitute.For<ClubMemberNotifier>();

        // !! START HERE !!
        // Do you want to use a test double for the "ClubMemberFactory"? Or would you rather provide the handler a
        // concrete instance of "ClubMemberFactory"? Just relax. There's not really a "right" answer. Either approach
        // has its advantages and disadvantages.
        // Be mindful about your choice while implementing the next couple of tests.
        _clubMemberFactory = null;

        _sut = new RegisterClubMemberHandler(_clubRepository, _clubMemberRepository, _clubMemberFactory, _clubMemberNotifier);
    }
    
    [Test]
    public void RegisterClubMember_ShouldPersistNewClubMember() 
    {
        // The happy path scenario - part 1
        // So far, we've been writing "State Verification" tests. For this test case, we're going to give "Behaviour Verification"
        // a try. The purpose of this test is to verify that a newly created `ClubMember` is saved by the `ClubMemberRepository`.
        //
        // Tip: An instance of a `Club` can be retrieved using the `ClubRepository`.
        // Tip: An instance of a `ClubMember` can be created using the `ClubMemberFactory`.
    }
    
    [Test]
    public void RegisterClubMember_ShouldSendPaymentRequestForMembershipFee() 
    {
        // The happy path scenario - part 2
        // We're going to write yet another "Behaviour Verification" test. The purpose of this test is to verify that
        // a payment request is sent to the new club member using the `ClubMemberNotifier`.
    }
    
    [Test]
    public void RegisterClubMember_ShouldIndicateSuccess() 
    {
        // The happy path scenario - part 3
        // This "State Verification" test verifies whether the return value indicates a successful operation.
    }
    
    [Test]
    public void RegisterClubMember_UnknownClub_ShouldIndicateFailure() 
    {
        // Edge case #1
        // When the specified club doesn't exist, then the result of the `Handle` method should indicate a failed
        // operation by returning a domain violation.
        //
        // Tip: You can use the `UnknownClubViolation` static field as the domain violation message.
    }
    
    [Test]
    public void RegisterClubMember_ClubMemberAlreadyRegistered_ShouldIndicateFailure() 
    {
        // Edge case #2
        // When the club member has already been registered, then the result of the `Handle` method should indicate a
        // failed operation by returning a domain violation.
        //
        // Tip: An instance of a `ClubMember` can be retrieved using the `ClubMemberRepository`.
        // Tip: You can use the `ClubMemberAlreadyRegisteredViolation` static field as the domain violation message.
    }
    
    [Test]
    public void RegisterClubMember_RegistrationFailure_ShouldIndicateFailure() 
    {
        // Edge case #3
        // When the `ClubMemberFactory` returns a domain violation (the right value of the tuple), then the result of the
        // `Handle` method should indicate a failed operation by returning the domain violation of the factory.
    }
}