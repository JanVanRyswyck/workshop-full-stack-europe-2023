using System.Net;
using System.Net.Http.Json;
using NUnit.Framework;
using SoccerClub.Api.ClubMembers;
using SoccerClub.Domain.Clubs;
using SoccerClub.DomainPorts.Clubs;
using SoccerClub.Tests.Common.Database;
using SoccerClub.Tests.Common.Host;

namespace SoccerClub.Tests.Exercise1_1.Sociable;

public class RegisterClubMemberTests
{
    private const string UuidPattern = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";

    private SoccerClubApplication _testApplication;
    private SoccerClubDatabase _soccerClubDatabase;
    private ClubRepository _clubRepository;
    private HttpClient _testHttpClient;

    [SetUp]
    public void SetUp()
    {
        _testApplication = new SoccerClubApplication();
        
        _soccerClubDatabase = _testApplication.GetRequiredService<SoccerClubDatabase>();
        _soccerClubDatabase.Initialise()
            .EnforceForeignKeys();
        
        _clubRepository = _testApplication.GetRequiredService<ClubRepository>();
        _testHttpClient = _testApplication.CreateClient();
    }

    [TearDown]
    public void TearDown()
    {
        _soccerClubDatabase.Cleanup();
        _testApplication.Dispose();
    }
    
    [Test]
    public async Task Should_receive_an_OK_response_together_with_the_identifier_of_the_newly_registered_club_member()
    {
        _clubRepository.Save(new Club(6354, "FC Barcelona"));
        
        var request = new RegisterClubMemberRequest
        {
            FirstName = "Andres",
            LastName = "Iniesta",
            Email = "andres@iniesta.es",
            BirthDate = new DateTime(1984, 5, 11),
            NationalIdentificationNumber = "84051135476"
        };
            
        var response = await _testHttpClient.PostAsJsonAsync("/api/clubs/6354/members", request);

        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var body = response.Content.ReadFromJsonAsync<RegisterClubMemberResponse>().Result;
        Assert.That(body, Is.Not.Null);
        Assert.That(body.ClubMemberId, Is.Not.Empty);
        Assert.That(body.ClubMemberId.ToString(), Does.Match(UuidPattern));
    }
}