using NodaTime;
using SoccerClub.Domain.ClubMembers;
using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.ClubMembers;
using SoccerClub.Tests.Common;

namespace SoccerClub.Tests.Exercise1_2.Solitary;

//
// For this test suite we're going to apply the  "Arrange, Act, Assert per test class" approach.
//

// The happy path scenario.
// When assigning a certification, a new `Certification` should be added to the `ClubMember`. After that, the
// `ClubMember` instance should be saved in the repository. We should also send a message to the club member
// congratulating her/him for this amazing accomplishment. This implies that we're going to make multiple
// observations.
public class When_assigning_a_certification_to_a_club_member
{
    [Establish]
    public void Context()
    {
        // Arrange stage
        // Create an instance of a `AssignCertificationHandler` (SUT) and everything that is necessary for executing
        // the scenario.
        //
        // Tip: An instance of a `ClubMember` can be retrieved using the `ClubRepository`.
    }
    
    [Because]
    public void Of()
    {
        // Act stage
        // Execute the `Handle` method on the `AssignCertificationHandler` instance created in `Context`.
    }
    
    [Observation]
    public void Then_the_club_member_should_have_a_new_certification()
    {
        // Assert stage - part 1
        // Observe that a new `Certification` has been added to the `Certifications` collection of the `ClubMember`
        // instance.
    }
    
    [Observation]
    public void Then_the_club_member_should_be_persisted()
    {
        // Assert stage - part 2
        // Observe that the `ClubMember` instance is saved by the `ClubMemberRepository`.
    }
    
    [Observation]
    public void Then_a_congratulatory_message_should_be_send_to_the_club_member()
    {
        // Assert stage - part 3
        // Observe that a congratulatory message is sent to the club member using the `ClubMemberNotifier`.
    }
    
    [Observation]
    public void Then_it_should_indicate_a_successful_operation()
    {
        // Assert stage - part 4
        // Observe whether the return value indicates a successful operation.
    }
    
    private ClubMember _clubMember;
    private Result _result;
        
    private ClubMemberRepository _clubMemberRepository;
    private ClubMemberNotifier _clubMemberNotifier;
    private AssignCertificationHandler _sut;
}

// Edge case #1
// When the specified club member doesn't exist, then the result of the `Handle` method should indicate a failed
// operation by returning a domain violation.
public class When_assigning_a_certification_to_an_unknown_club_member
{
    [Establish]
    public void Context()
    {
        // Arrange stage
        // Create an instance of a `AssignCertificationHandler` (SUT) and everything that is necessary for executing
        // the scenario.
    }
    
    [Because]
    public void Of()
    {
        // Act stage
        // Execute the `Handle` method on the `AssignCertificationHandler` instance created in `Context`.
    }
    
    [Observation]
    public void Then_it_should_indicate_a_failed_operation()
    {
        // Assert stage - part 1
        // Observe whether the return value indicates a failed operation.
    }
    
    [Observation]
    public void Then_the_result_should_specify_a_corresponding_domain_violation()
    {
        // Assert stage - part 2
        // Observe whether the return value contains the `UnknownClubMemberViolation` domain violation.
    }
    
    private Result _result;
    private AssignCertificationHandler _sut;
}

// Edge case #2
// When assigning a certification to a club member ends up with a domain violation, then the result of the
// `Handle` method should indicate a failed operation by returning that domain violation.
public class When_assigning_a_certification_to_a_club_member_results_in_a_failure
{
    [Establish]
    public void Context()
    {
        // Arrange stage
        // Create an instance of a `AssignCertificationHandler` (SUT) and everything that is necessary for executing
        // the scenario.
        //
        // Tip: The `Clock` to retrieve the current date.
    }
    
    [Because]
    public void Of()
    {
        // Act stage
        // Execute the `Handle` method on the `AssignCertificationHandler` instance created in `Context`.
    }
    
    [Observation]
    public void Then_it_should_indicate_a_failed_operation()
    {
        // Assert stage - part 1
        // Observe whether the return value indicates a failed operation.
    }
    
    [Observation]
    public void Then_the_result_should_specify_a_corresponding_domain_violation()
    {
        // Assert stage - part 2
        // Observe whether the return value contains the `InvalidIssueDateViolation` domain violation.
    }
    
    private Result _result;
    private AssignCertificationHandler _sut;
}