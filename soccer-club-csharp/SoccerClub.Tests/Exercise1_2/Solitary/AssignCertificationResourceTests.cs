using DeepEqual.Syntax;
using Microsoft.AspNetCore.Mvc;
using NodaTime;
using NSubstitute;
using NUnit.Framework;
using SoccerClub.Api.ClubMembers;
using SoccerClub.DomainPorts;
using SoccerClub.DomainPorts.ClubMembers;

namespace SoccerClub.Tests.Exercise1_2.Solitary;

public class AssignCertificationResourceTests
{
    private CommandHandler<AssignCertificationCommand> _assignCertificationHandler;
    private AssignCertificationResource _sut;

    [SetUp]
    public void SetUp()
    {
        _assignCertificationHandler = Substitute.For<CommandHandler<AssignCertificationCommand>>();
        _sut = new AssignCertificationResource(_assignCertificationHandler);
    }
    
    [Test]
    public void AssignCertification_ValidRequest_ShouldDispatchCommandToDomain()
    {
        AssignCertificationCommand dispatchedCommand = null;
        _assignCertificationHandler.Handle(Arg.Do<AssignCertificationCommand>(captured => dispatchedCommand = captured))
            .Returns(Result.Success());
        
        var request = new AssignCertificationRequest
        { 
            CertificateCode = "INITIATOR", 
            IssueDate = new DateTime(2021, 07, 27)
        };
        
        _sut.AssignCertification(5342, Guid.Parse("75595c84-ef7d-11eb-be6e-acde48001122"), request);
        
        var expectedCommand = new AssignCertificationCommand(5342, Guid.Parse("75595c84-ef7d-11eb-be6e-acde48001122"),
            "INITIATOR", new LocalDate(2021, 7, 27));

        dispatchedCommand.ShouldDeepEqual(expectedCommand);
    }
    
    [Test]
    public void AssignCertification_ValidRequest_ShouldRespondWithStatusCodeOK()
    {
        _assignCertificationHandler.Handle(Arg.Any<AssignCertificationCommand>()).Returns(Result.Success());

        var request = new AssignCertificationRequest
        {
            CertificateCode = "INITIATOR", 
            IssueDate = new DateTime(2021, 07, 27)
        };
        
        var response = _sut.AssignCertification(5342, Guid.Parse("75595c84-ef7d-11eb-be6e-acde48001122"), request);
        Assert.That(response, Is. TypeOf<OkResult>());
    }
    
    [Test]
    public void AssignCertification_InvalidRequest_ShouldRespondWithStatusCodeBadRequest()
    {
        var invalidRequest = new AssignCertificationRequest { CertificateCode = null, IssueDate = null };

        var response = _sut.AssignCertification(
            5342, Guid.Parse("75595c84-ef7d-11eb-be6e-acde48001122"), invalidRequest);

        Assert.That(response, Is.TypeOf<BadRequestResult>());
    }
    
    [Test]
    public void AssignCertification_RequestDoesNotMeetDomainCriteria_ShouldRespondWithStatusCodeBadRequest()
    {
        _assignCertificationHandler.Handle(Arg.Any<AssignCertificationCommand>())
            .Returns(Result.Failure(new DomainViolation("Request does not meet domain criteria")));

        var requestToBeRejected = new AssignCertificationRequest 
        {
            CertificateCode = "START2COACH", 
            IssueDate = new DateTime(2021, 07, 27)
        };

        var response = _sut.AssignCertification(
            5342, Guid.Parse("75595c84-ef7d-11eb-be6e-acde48001122"), requestToBeRejected);
        Assert.That(response, Is.TypeOf<BadRequestResult>());
    }
}