using SoccerClub.Domain.ClubMembers;
using SoccerClub.DomainPorts;
using SoccerClub.Tests.Common;

namespace SoccerClub.Tests.Exercise1_2.Solitary;

//
// For this test suite we're going to apply the  "Arrange, Act, Assert per test class" approach.
//

// The happy path scenario.
// When assigning a certification, then it should be added to the list of certifications.
public class When_assigning_a_certification
{
    [Establish]
    public void Context()
    {
        // Arrange stage
        // Create an instance of a `ClubMember` (SUT) and everything that is necessary for executing the scenario.
    }
    
    [Because]
    public void Of()
    {
        // Act stage
        // Execute the `AssignCertification` method on the `ClubMember` instance created in `Context`.
    }
    
    [Observation]
    public void Then_the_club_member_should_have_a_new_certification()
    {
        // Assert stage
        // Observe that a new `Certification` has been added to the `Certifications` list.
    }
    
    private Clock _clock;
    private ClubMember _sut;
}

// Edge case #1
// Assigning a certification with the same code should be an idempotent operation; meaning that there should be
// only a single certification in the list. The first assigned certification should be kept.
public class When_assigning_the_same_certification_multiple_times
{
    [Establish]
    public void Context()
    {
        // Arrange stage
        // Create an instance of a `ClubMember` (SUT) and everything that is necessary for executing the scenario.
    }
    
    [Because]
    public void Of()
    {
        // Act stage
        // Execute the `AssignCertification` method on the `ClubMember` instance.
    }
    
    [Observation]
    public void Then_the_club_member_should_only_have_the_first_assigned_certification()
    {
        // Assert stage
        // Observe that there's only a single `Certification` in the `Certifications` list. This should be the
        // certification that got assigned first.
    }
    
    private Clock _clock;
    private ClubMember _sut;
}

// Edge case #2
// Assigning a certification with an issue date in the future is not allowed. In that case, a domain violation
// should be issued and no new certification should be added to the list. This implies that we're going to make
// two observations.
public class When_assigning_a_certification_with_an_issue_date_in_the_future 
{
    [Establish]
    public void Context()
    {
        // Arrange stage
        // Create an instance of a `ClubMember` (SUT) and everything that is necessary for executing the scenario.
        //
        // Tip: The `Clock` to retrieve the current date.
    }
    
    [Because]
    public void Of()
    {
        // Act stage
        // Execute the `AssignCertification` method on the `ClubMember` instance, specifying a date that comes after
        // the current date specified in `Context`.        
    }
    
    [Observation]
    public void Then_it_should_result_in_a_domain_violation()
    {
        // Assert stage - part 1
        // Observe whether the `InvalidIssueDateViolation` has been added to the `DomainViolations` list.
    }
    
    [Observation]
    public void Then_no_certification_should_be_assigned() {

        // Assert stage - part 2
        // Observe that there's no new `Certification` in the `Certifications` list.
    }
    
    private Clock _clock;
    private ClubMember _sut;
}