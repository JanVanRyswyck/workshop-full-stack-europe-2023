using NUnit.Framework;
using SoccerClub.Api.ClubMembers;

namespace SoccerClub.Tests.Exercise1_2.Solitary;

public class AssignCertificationRequestValidatorTests
{
    [Test]
    public void Validate_ValidRequest_ShouldIndicatePositiveResult()
    {
        var validRequest = new AssignCertificationRequest
        {
            CertificateCode = "UEFA-A", 
            IssueDate = new DateTime(2021, 07, 27 )
        };
        
        var isValid = new AssignCertificationRequestValidator().Validate(validRequest);
        Assert.That(isValid);
    }
    
    [TestCase(null)]
    [TestCase("")]
    [TestCase(" ")]
    public void Validate_RequestWithoutCertificateCode_ShouldIndicateNegativeResult(string invalidCertificateCode)
    {
        var invalidRequest = new AssignCertificationRequest 
        {
            CertificateCode = invalidCertificateCode, 
            IssueDate = new DateTime(2021, 07, 02)
        };
        
        var isValid = new AssignCertificationRequestValidator().Validate(invalidRequest);
        Assert.That(isValid, Is.False);
    }
    
    [Test]
    public void Validate_RequestWithoutIssueDate_ShouldIndicateNegativeResult()
    {
        var invalidRequest = new AssignCertificationRequest 
        {
            CertificateCode = "UEFA-A", 
            IssueDate = null
        };
        
        var isValid = new AssignCertificationRequestValidator().Validate(invalidRequest);
        Assert.That(isValid, Is.False);
    }
}