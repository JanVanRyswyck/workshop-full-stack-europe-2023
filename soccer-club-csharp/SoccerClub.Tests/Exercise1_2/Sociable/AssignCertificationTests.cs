using System.Net;
using System.Net.Http.Json;
using NodaTime;
using NUnit.Framework;
using SoccerClub.Api.ClubMembers;
using SoccerClub.Domain.ClubMembers;
using SoccerClub.Domain.Clubs;
using SoccerClub.DomainPorts.ClubMembers;
using SoccerClub.DomainPorts.Clubs;
using SoccerClub.Tests.Common.Database;
using SoccerClub.Tests.Common.Host;

namespace SoccerClub.Tests.Exercise1_2.Sociable;

public class AssignCertificationTests
{
    private SoccerClubApplication _testApplication;
    private SoccerClubDatabase _soccerClubDatabase;
    private ClubRepository _clubRepository;
    private ClubMemberRepository _clubMemberRepository;
    private HttpClient _testHttpClient;

    [SetUp]
    public void SetUp()
    {
        _testApplication = new SoccerClubApplication();
        
        _soccerClubDatabase = _testApplication.GetRequiredService<SoccerClubDatabase>();
        _soccerClubDatabase.Initialise()
            .EnforceForeignKeys();
        
        _clubRepository = _testApplication.GetRequiredService<ClubRepository>();
        _clubMemberRepository = _testApplication.GetRequiredService<ClubMemberRepository>();
        _testHttpClient = _testApplication.CreateClient();
    }

    [TearDown]
    public void TearDown()
    {
        _soccerClubDatabase.Cleanup();
        _testApplication.Dispose();
    }
    
    [Test]
    public async Task Should_receive_an_OK_response()
    {
        var club = new Club(6354, "FC Barcelona");
        _clubRepository.Save(club);
        
        _clubMemberRepository.Save(new ClubMember(Guid.Parse("9fac088e-ef13-11eb-9d46-acde48001122"), club,
            "Andres", "Iniesta", "andres@iniesta.es", new LocalDate(1984, 05, 11),
            "84051135476"));

        var request = new AssignCertificationRequest
        {
            CertificateCode = "UEFA-B",
            IssueDate = new DateTime(2021, 07, 27)
        };

        var response = await _testHttpClient.PutAsJsonAsync(
            "/api/clubs/6354/members/9fac088e-ef13-11eb-9d46-acde48001122/certifications",
            request);
        
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
    }
}