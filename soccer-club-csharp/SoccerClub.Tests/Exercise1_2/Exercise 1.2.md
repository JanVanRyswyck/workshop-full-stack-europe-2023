# Exercise 1.2

![](Exercise_1.2.png)

## 1. Solitary tests

### 1.1 ClubMember

Now we're going to apply the "AAA Per Test Class" approach.

1. Open the files `ClubMember.java` and `AssignCertificationToClubMemberTests.java`. Complete the implementation of the 
   provided test cases using a Test-First approach.
2. Implement only a single test case for a single scenario at a time. Make sure that the test fails.
3. Provide the least amount of code to make the test pass.
4. Refactor, refactor, refactor!
5. Repeat these steps until all the tests pass.

### 1.2 AssignCertificationHandler

Here we're going to apply the "AAA Per Test Class" approach once more.

1. Open the files `AssignCertificationHandler.java` and `AssignCertificationHandlerTests.java`.
2. Complete the implementation of the provided test scenarios using a Test-First approach. Remember to only implement a 
   single test case at a time.

## 2. Sociable tests

Open the file `AssignCertificationTests.java`. There's a single Sociable test that calls the API endpoint for assigning
a certification to a club member. The test verifies the status code of the HTTP response. Go ahead and execute this test.
At this point, the test should be green as you implemented all the missing pieces in the previous steps.

There's nothing that should be done for this Sociable test as it has already been completely implemented. You can proceed
to the next step.

## 3. Open questions

* Now that you've seen both "AAA Per Test Method", and "AAA Per Test Class" in action, what are your thoughts about these 
  approaches? 
* What's your favorite style for naming tests? Why?
