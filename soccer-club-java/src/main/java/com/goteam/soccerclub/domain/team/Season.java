package com.goteam.soccerclub.domain.team;

public class Season {

    private final int startYear;
    private final int endYear;

    public Season(int startYear) {

        this.startYear = startYear;
        this.endYear = startYear + 1;
    }

    public int getStartYear() {
        return startYear;
    }
}
