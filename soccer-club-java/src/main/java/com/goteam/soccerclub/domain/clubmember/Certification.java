package com.goteam.soccerclub.domain.clubmember;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDate;

public class Certification {

    private final String certificateCode;
    private final LocalDate issueDate;

    public Certification(String certificateCode, LocalDate issueDate) {

        this.certificateCode = certificateCode;
        this.issueDate = issueDate;
    }

    public String getCertificateCode() {
        return certificateCode;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Certification that = (Certification) o;

        return new EqualsBuilder()
            .append(certificateCode, that.certificateCode)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(certificateCode)
            .toHashCode();
    }
}
