package com.goteam.soccerclub.domain.clubmember;

import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domainports.Clock;
import com.goteam.soccerclub.domainports.DomainViolation;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.UUID;

@Component
public class ClubMemberFactory {

    private final static int MINIMUM_AGE = 5;
    public final static DomainViolation MINIMUM_AGE_VIOLATION =
        new DomainViolation(String.format("A club member must be at least %d years old.", MINIMUM_AGE));

    private final Clock clock;

    @Autowired
    public ClubMemberFactory(Clock clock) {
        this.clock = clock;
    }

    public Pair<ClubMember, DomainViolation> createClubMember(FactoryData data) {
        throw new NotImplementedException();
    }

    public static class FactoryData {

        private final UUID clubMemberId;
        private final Club club;
        private final String firstName;
        private final String lastName;
        private final String email;
        private final LocalDate birthDate;
        private final String nationalIdentificationNumber;

        public FactoryData(UUID clubMemberId, Club club, String firstName, String lastName, String email,
                           LocalDate birthDate, String nationalIdentificationNumber) {
            this.clubMemberId = clubMemberId;
            this.club = club;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.birthDate = birthDate;
            this.nationalIdentificationNumber = nationalIdentificationNumber;
        }

        public Club getClub() {
            return club;
        }

        public UUID getClubMemberId() {
            return clubMemberId;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getEmail() {
            return email;
        }

        public LocalDate getBirthDate() {
            return birthDate;
        }

        public String getNationalIdentificationNumber() {
            return nationalIdentificationNumber;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            FactoryData that = (FactoryData) o;

            return new EqualsBuilder()
                .append(club, that.club)
                .append(clubMemberId, that.clubMemberId)
                .append(firstName, that.firstName)
                .append(lastName, that.lastName)
                .append(email, that.email)
                .append(birthDate, that.birthDate)
                .append(nationalIdentificationNumber, that.nationalIdentificationNumber)
                .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                .append(club)
                .append(clubMemberId)
                .append(firstName)
                .append(lastName)
                .append(email)
                .append(birthDate)
                .append(nationalIdentificationNumber)
                .toHashCode();
        }
    }
}
