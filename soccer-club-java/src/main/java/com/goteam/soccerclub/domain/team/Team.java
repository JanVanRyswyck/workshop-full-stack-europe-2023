package com.goteam.soccerclub.domain.team;

import com.goteam.soccerclub.domain.club.Club;

import java.util.UUID;

public class Team {

    private final UUID id;
    private final int clubNumber;
    private final String name;
    private final Season season;

    public Team(UUID id, Club club, String name, Season season) {

        this.id = id;
        this.clubNumber = club.getMasterNumber();
        this.name = name;
        this.season = season;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getClubNumber() {
        return clubNumber;
    }

    public Season getSeason() {
        return season;
    }
}
