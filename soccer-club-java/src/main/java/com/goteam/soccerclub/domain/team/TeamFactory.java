package com.goteam.soccerclub.domain.team;

import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domainports.Clock;
import com.goteam.soccerclub.domainports.DomainViolation;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class TeamFactory {

    public final static DomainViolation TEAM_COMPOSITION_PERIOD_VIOLATION =
        new DomainViolation("A team can only be composed between May 1 and August 31.");

    private final Clock clock;

    @Autowired
    public TeamFactory(Clock clock) {
        this.clock = clock;
    }

    public Pair<Team, DomainViolation> createTeam(FactoryData data) {
        // Tip: The `Clock` can be used to retrieve the current date.
        // The year of the current date can be used to create an instance of the current `Season`.
        throw new NotImplementedException();
    }

    public static class FactoryData {
        private final UUID id;
        private final Club club;
        private final String name;

        public FactoryData(UUID id, Club club, String name) {
            this.id = id;
            this.club = club;
            this.name = name;
        }

        public UUID getId() {
            return id;
        }

        public Club getClub() {
            return club;
        }

        public String getName() {
            return name;
        }
    }
}
