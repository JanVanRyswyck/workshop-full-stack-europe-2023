package com.goteam.soccerclub.domain.club;

public class Club {

    private final int masterNumber;
    private final String name;

    public Club(int masterNumber, String name) {
        this.masterNumber = masterNumber;
        this.name = name;
    }

    public int getMasterNumber() {
        return masterNumber;
    }

    public String getName() {
        return name;
    }
}
