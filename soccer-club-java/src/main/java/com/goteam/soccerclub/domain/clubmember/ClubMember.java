package com.goteam.soccerclub.domain.clubmember;

import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domainports.Clock;
import com.goteam.soccerclub.domainports.DomainViolation;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ClubMember {

    public final static DomainViolation INVALID_ISSUE_DATE_VIOLATION =
        new DomainViolation("The specified issue date cannot be in the future.");

    private final UUID id;
    private final int clubNumber;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final LocalDate birthDate;
    private final String nationalIdentificationNumber;

    private final List<Certification> certifications;
    private final List<DomainViolation> domainViolations;

    public ClubMember(UUID id, Club club, String firstName, String lastName, String email, LocalDate birthDate,
                      String nationalIdentificationNumber) {
        this.id = id;
        this.clubNumber = club.getMasterNumber();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthDate = birthDate;
        this.nationalIdentificationNumber = nationalIdentificationNumber;

        certifications = new ArrayList<>();
        domainViolations = new ArrayList<>();
    }

    public UUID getId() {
        return id;
    }

    public int getClubNumber() {
        return clubNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getNationalIdentificationNumber() {
        return nationalIdentificationNumber;
    }

    public Iterable<Certification> getCertifications() {
        return certifications;
    }

    public Iterable<DomainViolation> getDomainViolations() {
        return domainViolations;
    }

    public void assignCertification(String certificateCode, LocalDate issueDate, Clock clock) {
        //
        // Not implemented yet ...
        //
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClubMember that = (ClubMember) o;

        return new EqualsBuilder()
            .append(id, that.id)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(id)
            .toHashCode();
    }
}
