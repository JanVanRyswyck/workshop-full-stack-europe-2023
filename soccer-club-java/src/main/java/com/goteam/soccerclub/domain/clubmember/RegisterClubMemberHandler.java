package com.goteam.soccerclub.domain.clubmember;

import com.goteam.soccerclub.domainports.CommandHandler;
import com.goteam.soccerclub.domainports.DomainViolation;
import com.goteam.soccerclub.domainports.Result;
import com.goteam.soccerclub.domainports.club.ClubRepository;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberNotifier;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberRepository;
import com.goteam.soccerclub.domainports.clubmember.RegisterClubMemberCommand;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RegisterClubMemberHandler implements CommandHandler<RegisterClubMemberCommand> {

    public final static DomainViolation UNKNOWN_CLUB_VIOLATION =
        new DomainViolation("The specified club could not be found.");
    public final static DomainViolation CLUB_MEMBER_ALREADY_REGISTERED_VIOLATION =
        new DomainViolation("The club member has already been registered.");

    private final ClubRepository clubRepository;
    private final ClubMemberRepository clubMemberRepository;
    private final ClubMemberFactory clubMemberFactory;
    private final ClubMemberNotifier clubMemberNotifier;

    @Autowired
    public RegisterClubMemberHandler(ClubRepository clubRepository,
                                     ClubMemberRepository clubMemberRepository,
                                     ClubMemberFactory clubMemberFactory,
                                     ClubMemberNotifier clubMemberNotifier) {
        this.clubRepository = clubRepository;
        this.clubMemberRepository = clubMemberRepository;
        this.clubMemberFactory = clubMemberFactory;
        this.clubMemberNotifier = clubMemberNotifier;
    }

    @Override
    public Result handle(RegisterClubMemberCommand command) {
        throw new NotImplementedException();
    }
}
