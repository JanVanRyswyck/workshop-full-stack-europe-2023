package com.goteam.soccerclub.domain.clubmember;

import com.goteam.soccerclub.domainports.Clock;
import com.goteam.soccerclub.domainports.CommandHandler;
import com.goteam.soccerclub.domainports.DomainViolation;
import com.goteam.soccerclub.domainports.Result;
import com.goteam.soccerclub.domainports.clubmember.AssignCertificationCommand;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberNotifier;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberRepository;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AssignCertificationHandler implements CommandHandler<AssignCertificationCommand> {

    public final static DomainViolation UNKNOWN_CLUB_MEMBER_VIOLATION =
        new DomainViolation("The specified club member could not be found.");

    private final ClubMemberRepository clubMemberRepository;
    private final ClubMemberNotifier clubMemberNotifier;
    private final Clock clock;

    @Autowired
    public AssignCertificationHandler(ClubMemberRepository clubMemberRepository,
                                      ClubMemberNotifier clubMemberNotifier,
                                      Clock clock) {

        this.clubMemberRepository = clubMemberRepository;
        this.clubMemberNotifier = clubMemberNotifier;
        this.clock = clock;
    }

    @Override
    public Result handle(AssignCertificationCommand command) {
        throw new NotImplementedException();
    }
}
