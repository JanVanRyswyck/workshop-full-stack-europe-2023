package com.goteam.soccerclub.domain.team;

import com.goteam.soccerclub.domainports.CommandHandler;
import com.goteam.soccerclub.domainports.DomainViolation;
import com.goteam.soccerclub.domainports.Result;
import com.goteam.soccerclub.domainports.club.ClubRepository;
import com.goteam.soccerclub.domainports.team.MakeTeamCommand;
import com.goteam.soccerclub.domainports.team.TeamRepository;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MakeTeamHandler implements CommandHandler<MakeTeamCommand> {

    public final static DomainViolation UNKNOWN_CLUB_VIOLATION =
        new DomainViolation("The specified club could not be found.");

    private final ClubRepository clubRepository;
    private final TeamRepository teamRepository;
    private final TeamFactory teamFactory;

    @Autowired
    public MakeTeamHandler(ClubRepository clubRepository,
                           TeamRepository teamRepository,
                           TeamFactory teamFactory) {

        this.clubRepository = clubRepository;
        this.teamRepository = teamRepository;
        this.teamFactory = teamFactory;
    }

    @Override
    public Result handle(MakeTeamCommand command) {
        throw new NotImplementedException();
    }
}
