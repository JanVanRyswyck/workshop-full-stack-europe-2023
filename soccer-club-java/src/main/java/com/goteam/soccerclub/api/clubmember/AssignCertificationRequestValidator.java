package com.goteam.soccerclub.api.clubmember;

import org.apache.commons.lang3.StringUtils;

public class AssignCertificationRequestValidator {

    public boolean validate(AssignCertificationRequest request) {

        return hasValidCertificateCode(request) &&
            hasValidIssueDate(request);
    }

    private boolean hasValidCertificateCode(AssignCertificationRequest request) {
        return StringUtils.isNotBlank(request.getCertificateCode());
    }

    private boolean hasValidIssueDate(AssignCertificationRequest request) {
        return request.getIssueDate() != null;
    }
}