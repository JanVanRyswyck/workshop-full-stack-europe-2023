package com.goteam.soccerclub.api.team;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class MakeTeamResponse {

    private final UUID teamId;

    public MakeTeamResponse(@JsonProperty("teamId") UUID teamId) {
        this.teamId = teamId;
    }

    public UUID getTeamId() {
        return teamId;
    }
}
