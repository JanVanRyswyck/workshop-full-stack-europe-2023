package com.goteam.soccerclub.api.clubmember;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class RegisterClubMemberResponse {

    private final UUID clubMemberId;

    public RegisterClubMemberResponse(@JsonProperty("clubMemberId") UUID clubMemberId) {
        this.clubMemberId = clubMemberId;
    }

    public UUID getClubMemberId() {
        return clubMemberId;
    }
}
