package com.goteam.soccerclub.api.clubmember;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class RegisterClubMemberRequest {

    private final String firstName;
    private final String lastName;
    private final String email;
    private final LocalDate birthDate;
    private final String nationalIdentificationNumber;

    @JsonCreator
    public RegisterClubMemberRequest(@JsonProperty("firstName") String firstName,
                                     @JsonProperty("lastName") String lastName,
                                     @JsonProperty("email") String email,
                                     @JsonProperty("birthDate") LocalDate birthDate,
                                     @JsonProperty("nationalIdentificationNumber") String nationalIdentificationNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthDate = birthDate;
        this.nationalIdentificationNumber = nationalIdentificationNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getNationalIdentificationNumber() {
        return nationalIdentificationNumber;
    }
}
