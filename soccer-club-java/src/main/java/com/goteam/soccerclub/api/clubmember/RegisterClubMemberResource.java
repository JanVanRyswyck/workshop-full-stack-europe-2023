package com.goteam.soccerclub.api.clubmember;

import com.goteam.soccerclub.domainports.CommandHandler;
import com.goteam.soccerclub.domainports.Result;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberRepository;
import com.goteam.soccerclub.domainports.clubmember.RegisterClubMemberCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/clubs/{clubNumber}/members")
public class RegisterClubMemberResource {

    private final ClubMemberRepository repository;
    private final CommandHandler<RegisterClubMemberCommand> commandHandler;
    private final RegisterClubMemberRequestValidator validator;

    @Autowired
    public RegisterClubMemberResource(ClubMemberRepository repository,
                                      CommandHandler<RegisterClubMemberCommand> commandHandler) {
        this.repository = repository;
        this.commandHandler = commandHandler;
        this.validator = new RegisterClubMemberRequestValidator();
    }

    @PostMapping
    public ResponseEntity<RegisterClubMemberResponse> registerClubMember(@PathVariable int clubNumber,
                                                                         @RequestBody RegisterClubMemberRequest request) {

        var requestIsValid = validator.validate(request);
        if(! requestIsValid) {
            return ResponseEntity.badRequest().build();
        }

        var newClubMemberId = repository.getNextIdentity();
        var command = new RegisterClubMemberCommand(clubNumber, newClubMemberId, request.getFirstName(), request.getLastName(),
            request.getEmail(), request.getBirthDate(), request.getNationalIdentificationNumber());

        var result = commandHandler.handle(command);
        if(! result.isSuccessful())
            return ResponseEntity.badRequest().build();

        var response = new RegisterClubMemberResponse(newClubMemberId);
        return ResponseEntity.ok(response);
    }
}
