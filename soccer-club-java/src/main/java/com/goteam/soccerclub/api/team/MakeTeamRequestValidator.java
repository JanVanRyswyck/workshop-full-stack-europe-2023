package com.goteam.soccerclub.api.team;

import org.apache.commons.lang3.StringUtils;

public class MakeTeamRequestValidator {

    public Boolean validate(MakeTeamRequest request) {
        return hasValidName(request);
    }

    private boolean hasValidName(MakeTeamRequest request) {
        return StringUtils.isNotBlank(request.getName());
    }
}
