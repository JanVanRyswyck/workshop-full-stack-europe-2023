package com.goteam.soccerclub.api.team;

import com.goteam.soccerclub.domainports.CommandHandler;
import com.goteam.soccerclub.domainports.team.MakeTeamCommand;
import com.goteam.soccerclub.domainports.team.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/clubs/{clubNumber}/teams")
public class MakeTeamResource {

    private final TeamRepository repository;
    private final CommandHandler<MakeTeamCommand> commandHandler;
    private final MakeTeamRequestValidator validator;

    @Autowired
    public MakeTeamResource(TeamRepository repository,
                            CommandHandler<MakeTeamCommand> commandHandler) {
        this.repository = repository;
        this.commandHandler = commandHandler;
        validator = new MakeTeamRequestValidator();
    }

    @PostMapping
    public ResponseEntity<MakeTeamResponse> makeTeam(@PathVariable int clubNumber,
                                                     @RequestBody MakeTeamRequest request) {

        var requestIsValid = validator.validate(request);
        if(! requestIsValid) {
            return ResponseEntity.badRequest().build();
        }

        var newTeamId = repository.getNextIdentity();
        var command = new MakeTeamCommand(clubNumber, newTeamId, request.getName());

        var result = commandHandler.handle(command);
        if(! result.isSuccessful()) {
            return ResponseEntity.badRequest().build();
        }

        var response = new MakeTeamResponse(newTeamId);
        return ResponseEntity.ok(response);
    }
}
