package com.goteam.soccerclub.api.team;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MakeTeamRequest {

    private final String name;

    @JsonCreator
    public MakeTeamRequest(@JsonProperty("name") String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
