package com.goteam.soccerclub.api.clubmember;

import org.apache.commons.lang3.StringUtils;
import java.time.format.DateTimeFormatter;

public class RegisterClubMemberRequestValidator {

    private final static String NATIONAL_IDENTIFICATION_NUMBER_PATTERN = "^\\d{11}$";

    public boolean validate(RegisterClubMemberRequest request) {

        return hasValidFirstName(request) &&
            hasValidLastName(request) &&
            hasValidEmail(request) &&
            hasValidBirthDate(request) &&
            hasValidNationalIdentificationNumber(request);
    }

    private boolean hasValidFirstName(RegisterClubMemberRequest request) {
        return StringUtils.isNotBlank(request.getFirstName());
    }

    private boolean hasValidLastName(RegisterClubMemberRequest request) {
        return StringUtils.isNotBlank(request.getLastName());
    }

    private boolean hasValidEmail(RegisterClubMemberRequest request) {
        return StringUtils.isNotBlank(request.getEmail());
    }

    private boolean hasValidBirthDate(RegisterClubMemberRequest request) {
        return request.getBirthDate() != null;
    }

    private boolean hasValidNationalIdentificationNumber(RegisterClubMemberRequest request) {
        var birthDateSegment = request.getBirthDate().format(DateTimeFormatter.ofPattern("yyMMdd"));

        return StringUtils.isNotBlank(request.getNationalIdentificationNumber()) &&
            request.getNationalIdentificationNumber().matches(NATIONAL_IDENTIFICATION_NUMBER_PATTERN) &&
            request.getNationalIdentificationNumber().startsWith(birthDateSegment);
    }
}
