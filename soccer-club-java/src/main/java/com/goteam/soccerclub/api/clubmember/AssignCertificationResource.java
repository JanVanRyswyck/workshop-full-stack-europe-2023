package com.goteam.soccerclub.api.clubmember;

import com.goteam.soccerclub.domainports.CommandHandler;
import com.goteam.soccerclub.domainports.clubmember.AssignCertificationCommand;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/clubs/{clubNumber}/members/{clubMemberId}/certifications")
public class AssignCertificationResource {

    private final CommandHandler<AssignCertificationCommand> commandHandler;
    private final AssignCertificationRequestValidator validator;

    public AssignCertificationResource(CommandHandler<AssignCertificationCommand> commandHandler) {
        this.commandHandler = commandHandler;
        this.validator = new AssignCertificationRequestValidator();
    }

    @PutMapping
    public ResponseEntity<Void> assignCertification(@PathVariable int clubNumber,
                                                    @PathVariable UUID clubMemberId,
                                                    @RequestBody AssignCertificationRequest request) {

        var requestIsValid = validator.validate(request);
        if(! requestIsValid) {
            return ResponseEntity.badRequest().build();
        }

        var command = new AssignCertificationCommand(clubNumber, clubMemberId, request.getCertificateCode(),
            request.getIssueDate());

        var result = commandHandler.handle(command);
        if(! result.isSuccessful())
            return ResponseEntity.badRequest().build();

        return ResponseEntity.ok().build();
    }
}
