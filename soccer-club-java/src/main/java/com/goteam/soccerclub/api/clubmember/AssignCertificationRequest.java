package com.goteam.soccerclub.api.clubmember;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class AssignCertificationRequest {

    private final String certificateCode;
    private final LocalDate issueDate;

    @JsonCreator
    public AssignCertificationRequest(@JsonProperty("certificateCode") String certificateCode,
                                      @JsonProperty("issueDate") LocalDate issueDate) {

        this.certificateCode = certificateCode;
        this.issueDate = issueDate;
    }

    public String getCertificateCode() {
        return certificateCode;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }
}
