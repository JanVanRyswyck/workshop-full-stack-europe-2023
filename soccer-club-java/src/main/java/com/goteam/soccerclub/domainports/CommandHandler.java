package com.goteam.soccerclub.domainports;

public interface CommandHandler<TCommand> {
    Result handle(TCommand command);
}
