package com.goteam.soccerclub.domainports.team;

import com.goteam.soccerclub.domain.team.Team;

import java.util.UUID;

public interface TeamRepository {

    // Retrieves a team for a specified club and identifier.
    // When found, returns a `Team` instance; otherwise a null reference is returned.
    Team get(int clubNumber, UUID id);

    // Generates an identifier for a new `Team` instance.
    UUID getNextIdentity();

    // Saves the data of the specified `Team` instance.
    void save(Team team);
}
