package com.goteam.soccerclub.domainports.clubmember;

import com.goteam.soccerclub.domain.clubmember.ClubMember;

public interface ClubMemberNotifier {

    void sendPaymentRequestForMembershipFee(ClubMember clubMember);
    void sendCongratulationsForCertification(ClubMember clubMember);
}
