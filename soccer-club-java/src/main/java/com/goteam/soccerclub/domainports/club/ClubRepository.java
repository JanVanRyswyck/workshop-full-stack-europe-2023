package com.goteam.soccerclub.domainports.club;

import com.goteam.soccerclub.domain.club.Club;

public interface ClubRepository {

    // Retrieves a club for a specified master number.
    // When found, returns a `Club` instance; otherwise a null reference is returned.
    Club get(int masterNumber);

    // Saves the data of the specified `Club` instance.
    void save(Club club);
}
