package com.goteam.soccerclub.domainports.team;

import java.util.UUID;

public class MakeTeamCommand {

    private final int clubNumber;
    private final UUID teamId;
    private final String name;

    public MakeTeamCommand(int clubNumber, UUID teamId, String name) {
        this.clubNumber = clubNumber;
        this.teamId = teamId;
        this.name = name;
    }

    public int getClubNumber() {
        return clubNumber;
    }

    public UUID getTeamId() {
        return teamId;
    }

    public String getName() {
        return name;
    }
}
