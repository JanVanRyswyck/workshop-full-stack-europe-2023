package com.goteam.soccerclub.domainports.clubmember;

import java.time.LocalDate;
import java.util.UUID;

public class AssignCertificationCommand {

    private final int clubNumber;
    private final UUID clubMemberId;
    private final String certificateCode;
    private final LocalDate issueDate;

    public AssignCertificationCommand(int clubNumber, UUID clubMemberId, String certificateCode, LocalDate issueDate) {
        this.clubNumber = clubNumber;
        this.clubMemberId = clubMemberId;
        this.certificateCode = certificateCode;
        this.issueDate = issueDate;
    }

    public int getClubNumber() {
        return clubNumber;
    }

    public UUID getClubMemberId() {
        return clubMemberId;
    }

    public String getCertificateCode() {
        return certificateCode;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }
}
