package com.goteam.soccerclub.domainports;

public class DomainViolation {

    private String message;

    public DomainViolation(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
