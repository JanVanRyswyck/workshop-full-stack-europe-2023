package com.goteam.soccerclub.domainports;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Result {

    private final List<DomainViolation> domainViolations;
    private final Boolean successful;

    private Result(List<DomainViolation> domainViolations) {
        this.domainViolations = domainViolations;
        this.successful = domainViolations.isEmpty();
    }

    public List<DomainViolation> getDomainViolations() {
        return domainViolations;
    }

    public Boolean isSuccessful() {
        return successful;
    }

    public static Result success() {
        return new Result(Collections.emptyList());
    }

    public static Result failure(DomainViolation... domainViolations) {
        return new Result(Arrays.asList(domainViolations));
    }
}
