package com.goteam.soccerclub.domainports.clubmember;

import com.goteam.soccerclub.domain.clubmember.ClubMember;

import java.util.UUID;

public interface ClubMemberRepository {

    // Retrieves a club member for a specified club and identifier.
    // When found, returns a `ClubMember` instance; otherwise a null reference is returned.
    ClubMember get(int clubNumber, UUID id);

    // Retrieves a club member for a specified club and national identification number.
    // When found, returns a `ClubMember` instance; otherwise a null reference is returned.
    ClubMember get(int clubNumber, String nationalIdentificationNumber);

    // Generates an identifier for a new `ClubMember` instance.
    UUID getNextIdentity();

    // Saves the data of the specified `ClubMember` instance.
    void save(ClubMember clubMember);
}
