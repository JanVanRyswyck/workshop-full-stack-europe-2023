package com.goteam.soccerclub.domainports.clubmember;

import java.time.LocalDate;
import java.util.UUID;

public class RegisterClubMemberCommand {

    private final int clubNumber;
    private final UUID clubMemberId;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final LocalDate birthDate;
    private final String nationalIdentificationNumber;

    public RegisterClubMemberCommand(int clubNumber, UUID clubMemberId, String firstName, String lastName, String email,
                                     LocalDate birthDate, String nationalIdentificationNumber) {
        this.clubNumber = clubNumber;
        this.clubMemberId = clubMemberId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthDate = birthDate;
        this.nationalIdentificationNumber = nationalIdentificationNumber;
    }

    public int getClubNumber() {
        return clubNumber;
    }

    public UUID getClubMemberId() {
        return clubMemberId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getNationalIdentificationNumber() {
        return nationalIdentificationNumber;
    }
}
