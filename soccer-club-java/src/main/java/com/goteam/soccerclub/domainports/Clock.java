package com.goteam.soccerclub.domainports;

import java.time.LocalDate;

public interface Clock {
    LocalDate getCurrentDate();
}
