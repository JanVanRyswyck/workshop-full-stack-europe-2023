package com.goteam.soccerclub.infrastructure.team;

import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domain.team.Season;
import com.goteam.soccerclub.domain.team.Team;
import com.goteam.soccerclub.domainports.team.TeamRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

@Component
public class SQLiteTeamRepository implements TeamRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public SQLiteTeamRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Team get(int clubNumber, UUID id) {

        var sql = "SELECT Id, ClubNumber, Name, SeasonStartYear " +
                  "FROM Team " +
                  "WHERE Id = :id " +
                  "AND ClubNumber = :clubNumber";

        var parameters = Map.of(
            "id", id,
            "clubNumber", clubNumber);

        try {
            var result = jdbcTemplate.queryForObject(sql, parameters, SQLiteTeamRepository::mapTeamFromResultSet);
            if (result == null) {
                return null;
            }

            return result;
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private static Team mapTeamFromResultSet(ResultSet resultSet, int rowNumber) throws SQLException {

        var clubProxy = new Club(resultSet.getInt("ClubNumber"), null);
        var season = new Season(resultSet.getInt("SeasonStartYear"));

        return new Team(
            UUID.fromString(resultSet.getString("Id")),
            clubProxy, resultSet.getString("Name"),
            season
        );
    }

    @Override
    public UUID getNextIdentity() {
        return UUID.randomUUID();
    }

    @Override
    public void save(Team team) {

        if(exists(team)) {
            update(team);
        } else {
            insert(team);
        }
    }

    private boolean exists(Team team) {

        var sql = "SELECT 1 FROM Team WHERE Id = :id AND ClubNumber = :clubNumber";
        var parameters = Map.of(
            "id", team.getId(),
            "clubNumber", team.getClubNumber());

        try {
            var result = jdbcTemplate.queryForObject(sql, parameters, Integer.class);
            return result != null && result == 1;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    private void insert(Team team) {

        var sql = "INSERT INTO Team (Id, ClubNumber, Name, SeasonStartYear) " +
            "VALUES(:id, :clubNumber, :name, :seasonStartYear)";

        var parameters = Map.of(
            "id", team.getId(),
            "clubNumber", team.getClubNumber(),
            "name", team.getName(),
            "seasonStartYear", team.getSeason().getStartYear());

        jdbcTemplate.update(sql, parameters);
    }

    private void update(Team team) {

        var sql = "UPDATE Team " +
            "SET Name = :name, " +
            "SeasonStartYear = :seasonStartYear " +
            "WHERE Id = :id AND ClubNumber = :clubNumber";

        var parameters = Map.of(
            "name", team.getName(),
            "seasonStartYear", team.getSeason().getStartYear(),
            "id", team.getId(),
            "clubNumber", team.getClubNumber());

        jdbcTemplate.update(sql, parameters);
    }
}
