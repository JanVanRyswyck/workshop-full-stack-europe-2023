package com.goteam.soccerclub.infrastructure;

import com.goteam.soccerclub.domainports.Clock;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class SystemClock implements Clock {

    @Override
    public LocalDate getCurrentDate() {
        return LocalDate.now();
    }
}
