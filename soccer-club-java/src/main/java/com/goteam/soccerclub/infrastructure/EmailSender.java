package com.goteam.soccerclub.infrastructure;

import com.goteam.soccerclub.domain.clubmember.ClubMember;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberNotifier;
import org.springframework.stereotype.Component;

@Component
public class EmailSender implements ClubMemberNotifier {

    @Override
    public void sendPaymentRequestForMembershipFee(ClubMember clubMember) {
    }

    @Override
    public void sendCongratulationsForCertification(ClubMember clubMember) {
    }
}
