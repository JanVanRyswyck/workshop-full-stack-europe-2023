package com.goteam.soccerclub.infrastructure.clubmember;

import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domain.clubmember.Certification;
import com.goteam.soccerclub.domain.clubmember.ClubMember;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberRepository;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Map;
import java.util.UUID;

@Component
public class SQLiteClubMemberRepository implements ClubMemberRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public SQLiteClubMemberRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public ClubMember get(int clubNumber, UUID id) {

        var sql = "SELECT Id, ClubNumber, FirstName, LastName, Email, BirthDate, NationalIdentificationNumber " +
                  "FROM ClubMember " +
                  "WHERE Id = :id " +
                  "AND ClubNumber = :clubNumber";

        var parameters = Map.of(
            "id", id,
            "clubNumber", clubNumber);

        return queryClubMember(sql, parameters);
    }

    @Override
    public ClubMember get(int clubNumber, String nationalIdentificationNumber) {

        var sql = "SELECT Id, ClubNumber, FirstName, LastName, Email, BirthDate, NationalIdentificationNumber " +
            "FROM ClubMember " +
            "WHERE NationalIdentificationNumber = :nationalIdentificationNumber " +
            "AND ClubNumber = :clubNumber";

        var parameters = Map.of(
            "nationalIdentificationNumber", nationalIdentificationNumber,
            "clubNumber", clubNumber);

        return queryClubMember(sql, parameters);
    }

    @Override
    public UUID getNextIdentity() {
        return UUID.randomUUID();
    }

    @Override
    public void save(ClubMember clubMember) {

        if(IterableUtils.size(clubMember.getDomainViolations()) > 0) {
            throw new IllegalStateException("The specified club member cannot be saved due to domain violations.");
        }

        if(exists(clubMember)) {
            update(clubMember);
        } else {
            insert(clubMember);
        }
    }

    private ClubMember queryClubMember(String sql, Map<String, ? extends Serializable> parameters) {

        try {
            var result = jdbcTemplate.queryForObject(sql, parameters, SQLiteClubMemberRepository::mapClubMemberFromResultSet);
            if (result == null) {
                return null;
            }

            populateWithCertifications(result);
            return result;
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private static ClubMember mapClubMemberFromResultSet(ResultSet resultSet, int rowNumber) throws SQLException {

        var clubProxy = new Club(resultSet.getInt("ClubNumber"), null);
        var birthDate = LocalDate.parse(resultSet.getString("BirthDate"));

        return new ClubMember(
            UUID.fromString(resultSet.getString("Id")),
            clubProxy,
            resultSet.getString("FirstName"),
            resultSet.getString("LastName"),
            resultSet.getString("Email"),
            birthDate,
            resultSet.getString("NationalIdentificationNumber"));
    }

    private void populateWithCertifications(ClubMember clubMember) {

        var sql = "SELECT Code, IssueDate FROM ClubMemberCertification WHERE ClubMemberId = :clubMemberId";
        var parameters = Map.of("clubMemberId", clubMember.getId());

        var certifications = jdbcTemplate.query(sql, parameters, SQLiteClubMemberRepository::mapCertificationFromResultSet);

        var certificationsField = ReflectionUtils.findField(ClubMember.class, "certifications");
        ReflectionUtils.makeAccessible(certificationsField);
        ReflectionUtils.setField(certificationsField, clubMember, certifications);
    }

    private static Certification mapCertificationFromResultSet(ResultSet resultSet, int rowNumber) throws SQLException {

        return new Certification(
            resultSet.getString("Code"),
            LocalDate.parse(resultSet.getString("IssueDate"))
        );
    }

    private boolean exists(ClubMember clubMember) {

        var sql = "SELECT 1 FROM ClubMember WHERE Id = :id AND ClubNumber = :clubNumber";
        var parameters = Map.of(
            "id", clubMember.getId(),
            "clubNumber", clubMember.getClubNumber());

        try {
            var result = jdbcTemplate.queryForObject(sql, parameters, Integer.class);
            return result != null && result == 1;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    private void insert(ClubMember clubMember) {

        var sql = "INSERT INTO ClubMember (Id, ClubNumber, FirstName, LastName, Email, BirthDate, NationalIdentificationNumber) " +
            "VALUES(:id, :clubNumber, :firstName, :lastName, :email, :birthDate, :nationalIdentificationNumber)";

        var parameters = Map.of(
            "id", clubMember.getId(),
            "clubNumber", clubMember.getClubNumber(),
            "firstName", clubMember.getFirstName(),
            "lastName", clubMember.getLastName(),
            "email", clubMember.getEmail(),
            "birthDate", clubMember.getBirthDate(),
            "nationalIdentificationNumber", clubMember.getNationalIdentificationNumber());

        jdbcTemplate.update(sql, parameters);
    }

    private void update(ClubMember clubMember) {

        var sql = "UPDATE ClubMember " +
            "SET FirstName = :firstName, " +
            "LastName = :lastName, " +
            "Email = :email, " +
            "BirthDate = :birthDate, " +
            "NationalIdentificationNumber = :nationalIdentificationNumber " +
            "WHERE Id = :id AND ClubNumber = :clubNumber";

        var parameters = Map.of(
            "firstName", clubMember.getFirstName(),
            "lastName", clubMember.getLastName(),
            "email", clubMember.getEmail(),
            "birthDate", clubMember.getBirthDate(),
            "nationalIdentificationNumber", clubMember.getNationalIdentificationNumber(),
            "id", clubMember.getId(),
            "clubNumber", clubMember.getClubNumber());

        jdbcTemplate.update(sql, parameters);

        removeAllCertifications(clubMember);
        clubMember.getCertifications()
            .forEach(certification -> insert(certification, clubMember.getId(), clubMember.getClubNumber()));
    }

    private void removeAllCertifications(ClubMember clubMember) {

        var sql = "DELETE FROM ClubMemberCertification WHERE ClubMemberId = :clubMemberId AND ClubNumber = :clubNumber";
        var parameters = Map.of(
            "clubMemberId", clubMember.getId(),
            "clubNumber", clubMember.getClubNumber());

        jdbcTemplate.update(sql, parameters);
    }

    private void insert(Certification certification, UUID clubMemberId, int clubNumber) {

        var sql = "INSERT INTO ClubMemberCertification (Code, ClubMemberId, ClubNumber, IssueDate)" +
            "VALUES(:code, :clubMemberId, :clubNumber, :issueDate)";

        var parameters = Map.of(
            "code", certification.getCertificateCode(),
            "clubMemberId", clubMemberId,
            "clubNumber", clubNumber,
            "issueDate", certification.getIssueDate()
        );

        jdbcTemplate.update(sql, parameters);
    }
}
