package com.goteam.soccerclub.infrastructure.club;

import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domainports.club.ClubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

@Component
public class SQLiteClubRepository implements ClubRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public SQLiteClubRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Club get(int masterNumber) {

        var sql = "SELECT MasterNumber, Name FROM Club WHERE MasterNumber = :masterNumber";
        var parameters = Map.of("masterNumber", masterNumber);

        try {
            return jdbcTemplate.queryForObject(sql, parameters, SQLiteClubRepository::mapFromResultSet);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private static Club mapFromResultSet(ResultSet resultSet, int rowNumber) throws SQLException {
        return new Club(
            resultSet.getInt("MasterNumber"),
            resultSet.getString("Name"));
    }

    @Override
    public void save(Club club) {

        var sql = "INSERT INTO Club (MasterNumber, Name) VALUES(:masterNumber, :name)";
        var parameters = Map.of(
            "masterNumber", club.getMasterNumber(),
            "name", club.getName()
        );

        jdbcTemplate.update(sql, parameters);
    }
}
