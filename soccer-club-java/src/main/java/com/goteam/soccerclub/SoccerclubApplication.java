package com.goteam.soccerclub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoccerclubApplication {

    public static void main(String[] args) {
        SpringApplication.run(SoccerclubApplication.class, args);
    }

}
