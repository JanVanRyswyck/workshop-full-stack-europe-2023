CREATE TABLE Club (
    MasterNumber INTEGER NOT NULL,
    Name TEXT NOT NULL,
    PRIMARY KEY(MasterNumber)
) WITHOUT ROWID;

CREATE TABLE ClubMember (
    Id TEXT NOT NULL,
    ClubNumber INTEGER NOT NULL,
    FirstName TEXT NOT NULL,
    LastName TEXT NOT NULL,
    Email TEXT NOT NULL,
    BirthDate TEXT NOT NULL,
    NationalIdentificationNumber TEXT NOT NULL,
    PRIMARY KEY(Id, ClubNumber),
    UNIQUE(NationalIdentificationNumber),
    FOREIGN KEY(ClubNumber) REFERENCES Club(MasterNumber)
) WITHOUT ROWID;

CREATE TABLE ClubMemberCertification (
    Code TEXT NOT NULL,
    ClubMemberId TEXT NOT NULL,
    ClubNumber INTEGER NOT NULL,
    IssueDate TEXT NOT NULL,
    PRIMARY KEY(Code, ClubMemberId, ClubNumber),
    FOREIGN KEY(ClubMemberId, ClubNumber) REFERENCES ClubMember(Id, ClubNumber)
) WITHOUT ROWID;

CREATE TABLE Team (
    Id TEXT NOT NULL,
    ClubNumber INTEGER NOT NULL,
    Name TEXT NOT NULL,
    SeasonStartYear INTEGER NOT NULL,
    PRIMARY KEY(Id, ClubNumber),
    FOREIGN KEY(ClubNumber) REFERENCES Club(MasterNumber)
) WITHOUT ROWID;
