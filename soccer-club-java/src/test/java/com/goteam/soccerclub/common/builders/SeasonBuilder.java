package com.goteam.soccerclub.common.builders;

import com.goteam.soccerclub.domain.team.Season;
import org.apache.commons.lang3.NotImplementedException;

public class SeasonBuilder {

    private int startYear;

    public SeasonBuilder() {
        // Step 1: provide default values (test data) for the private field.
    }

    public SeasonBuilder withStartYear(int startYear) {
        // Step 2: override the default `startYear` with the specified value.
        throw new NotImplementedException();
    }

    public Season build() {
        // Step 3: build an instance of a `Season` using the private field.
        throw new NotImplementedException();
    }

    // Step 4: Go to the class `Example`, and add a factory method for creating an instance of a `SeasonBuilder`.
}
