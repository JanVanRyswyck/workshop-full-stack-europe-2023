package com.goteam.soccerclub.common.builders;

import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domain.team.Team;
import org.apache.commons.lang3.NotImplementedException;

import java.util.UUID;
import java.util.function.Consumer;

public class TeamBuilder {

    private UUID id;
    private String name;
    private SeasonBuilder season;
    private Club club;

    public TeamBuilder() {
        // Step 1: provide default values (test data) for the private field.
        // Use other test data builders for complex types (e.g. `Example.club()`).
    }

    public TeamBuilder withId(UUID id) {
        // Step 2: override the default `id` with the specified value.
        throw new NotImplementedException();
    }

    public TeamBuilder withName(String name) {
        // Step 3: override the default `name` with the specified value.
        throw new NotImplementedException();
    }

    public TeamBuilder withSeason(Consumer<SeasonBuilder> build) {
        // Step 4: by accepting a `Consumer<>`, we improve upon the readability of the test data builder DSL.
        // e.g. `.withSeason(season -> season.withStartYear(2021))`
        // A `Consumer<>` can be invoked by calling its `accept` method, specifying the necessary parameter.
        throw new NotImplementedException();
    }

    public TeamBuilder withClub(Club club) {
        // Step 5: sometimes it can also come in handy to just accept an instance of another complex object.
        // In that case we just assign the specified instance to the corresponding field.
        throw new NotImplementedException();
    }

    public Team build() {
        // Step 6: build an instance of a `Team` using the private fields.
        throw new NotImplementedException();
    }

    // Step 7: Go to the class `Example`, and add a factory method for creating an instance of a `TeamBuilder`.
}
