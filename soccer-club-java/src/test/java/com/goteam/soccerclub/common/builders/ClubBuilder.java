package com.goteam.soccerclub.common.builders;

import com.goteam.soccerclub.domain.club.Club;
import org.apache.commons.lang3.NotImplementedException;

public class ClubBuilder {

    private int masterNumber;
    private String name;

    public ClubBuilder() {
        // Step 1: provide default values (test data) for the private fields.
    }

    public ClubBuilder withMasterNumber(int masterNumber) {
        // Step 2: override the default `masterNumber` with the specified value.
        throw new NotImplementedException();
    }

    public ClubBuilder withName(String name) {
        // Step 3: override the default `name` with the specified value.
        throw new NotImplementedException();
    }

    public Club build() {
        // Step 4: Build an instance of a `Club` using the private fields.
        throw new NotImplementedException();
    }
}
