package com.goteam.soccerclub.common.fakes;

import com.goteam.soccerclub.domainports.Clock;

import java.time.LocalDate;

public class FakeClock implements Clock
{
    private final LocalDate currentDate;

    public FakeClock(LocalDate currentDate) {
        this.currentDate = currentDate;
    }

    @Override
    public LocalDate getCurrentDate() {
        return currentDate;
    }
}
