package com.goteam.soccerclub.common.builders;

import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domain.clubmember.Certification;
import com.goteam.soccerclub.domain.clubmember.ClubMember;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ClubMemberBuilder {

    private UUID id;
    private Club club;
    private String firstName;
    private String lastName;
    private String email;
    private LocalDate birthDate;
    private String nationalIdentificationNumber;

    public ClubMemberBuilder() {
        id = UUID.fromString("d8811d8f-ae7c-4ff9-a6c8-3815957f07ea");
        club = Example.club().build();
        firstName = "Robert";
        lastName = "Lewandowski";
        email = "robert@lewandowski.pl";
        birthDate = LocalDate.of(1988, 8, 21);
        nationalIdentificationNumber = "88082138475";
    }

    public ClubMemberBuilder withId(UUID id) {
        this.id = id;
        return this;
    }

    public ClubMemberBuilder withClub(Club club) {
        this.club = club;
        return this;
    }

    public ClubMemberBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public ClubMemberBuilder withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public ClubMemberBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public ClubMemberBuilder withBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public ClubMemberBuilder withNationalIdentificationNumber(String nationalIdentificationNumber) {
        this.nationalIdentificationNumber = nationalIdentificationNumber;
        return this;
    }

    public ClubMember build() {
        return new ClubMember(id, club, firstName, lastName, email, birthDate, nationalIdentificationNumber);
    }
}
