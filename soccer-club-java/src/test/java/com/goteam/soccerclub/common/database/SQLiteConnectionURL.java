package com.goteam.soccerclub.common.database;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

public class SQLiteConnectionURL {

    public static String build() {
        var sqliteDatabaseFile = new File(String.format("%s/soccer_club_test.db", getBaseDirectory()));
        return String.format("jdbc:sqlite:%s", sqliteDatabaseFile);
    }

    private static String getBaseDirectory() {

        var buildDirectory = String.format("%s/build/tmp/test", System.getProperty("user.dir"));
        if(Files.exists(Path.of(buildDirectory))) {
            return buildDirectory;
        }

        var outDirectory = String.format("%s/out/test", System.getProperty("user.dir"));
        if(Files.exists(Path.of(outDirectory))) {
            return outDirectory;
        }

        return System.getProperty("java.io.tmpdir");
    }
}
