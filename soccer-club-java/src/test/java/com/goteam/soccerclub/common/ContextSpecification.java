package com.goteam.soccerclub.common;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class ContextSpecification {

    @BeforeAll
    public void setUp() {
        establishContext();
        because();
    }

    protected abstract void establishContext();
    protected abstract void because();
}
