package com.goteam.soccerclub.common.database;

import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;

public class SoccerClubDatabase {

    private final JdbcTemplate jdbcTemplate;

    public SoccerClubDatabase(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public SoccerClubDatabase initialise() {
        var soccerClubSchemaResource = new ClassPathResource("sql/initialiseSchema.sql");
        ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator(soccerClubSchemaResource);
        databasePopulator.execute(getDataSource());
        return this;
    }

    public void cleanup() {
        var cleanupSchemaResource = new ClassPathResource("sql/cleanupSchema.sql");
        ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator(cleanupSchemaResource);
        databasePopulator.execute(getDataSource());
    }

    public void enforceForeignKeys() {
        jdbcTemplate.execute("PRAGMA foreign_keys = ON;");
    }

    private DataSource getDataSource() {
        return jdbcTemplate.getDataSource();
    }
}
