package com.goteam.soccerclub.common.database;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

public class SociableTestConfigurationInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {

        TestPropertyValues
            .of("spring.datasource.url=" + SQLiteConnectionURL.build())
            .applyTo(applicationContext);
    }
}
