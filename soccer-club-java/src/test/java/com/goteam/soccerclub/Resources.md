# Resources

* [JUnit documentation](https://junit.org/junit5/docs/current/user-guide/)
* [Mockito documentation](https://site.mockito.org)
* [AssertJ documentation](https://assertj.github.io/doc/)