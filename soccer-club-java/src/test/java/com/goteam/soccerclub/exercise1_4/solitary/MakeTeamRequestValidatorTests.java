package com.goteam.soccerclub.exercise1_4.solitary;

import com.goteam.soccerclub.api.team.MakeTeamRequest;
import com.goteam.soccerclub.api.team.MakeTeamRequestValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

public class MakeTeamRequestValidatorTests {

    @Test
    public void validate_validRequest_shouldIndicatePositiveResult() {

        var validRequest = new MakeTeamRequest("U11 A");
        var isValid = new MakeTeamRequestValidator().validate(validRequest);
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = { "", " " })
    public void validate_requestWithoutName_shouldIndicateNegativeResult(String invalidName) {

        var invalidRequest = new MakeTeamRequest(invalidName);
        var isValid = new MakeTeamRequestValidator().validate(invalidRequest);
        assertThat(isValid).isFalse();
    }
}
