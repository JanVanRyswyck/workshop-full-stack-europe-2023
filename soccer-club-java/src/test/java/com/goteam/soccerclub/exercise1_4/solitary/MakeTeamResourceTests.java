package com.goteam.soccerclub.exercise1_4.solitary;

import com.goteam.soccerclub.api.team.MakeTeamRequest;
import com.goteam.soccerclub.api.team.MakeTeamResource;
import com.goteam.soccerclub.api.team.MakeTeamResponse;
import com.goteam.soccerclub.common.ContextSpecification;
import com.goteam.soccerclub.domainports.CommandHandler;
import com.goteam.soccerclub.domainports.DomainViolation;
import com.goteam.soccerclub.domainports.Result;
import com.goteam.soccerclub.domainports.team.MakeTeamCommand;
import com.goteam.soccerclub.domainports.team.TeamRepository;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class MakeTeamResourceTests {

    @Nested
    class When_making_a_new_team
        extends ContextSpecification {

        @Override
        protected void establishContext() {

            fixture = new MakeTeamResourceFixture();
            SUT = fixture
                .sutCanQuery(TEAM_ID)
                .sutCanQuery(Result.success())
                .buildSUT();
        }

        @Override
        protected void because() {

            var request = new MakeTeamRequest("U13 B");
            response = SUT.makeTeam(4873, request);
        }

        @Test
        public void then_it_should_dispatch_a_command_to_the_domain() {

            var dispatchedCommand = fixture.interceptDispatchedCommand();
            var expectedCommand = new MakeTeamCommand(4873, TEAM_ID,"U13 B");
            assertThat(dispatchedCommand).usingRecursiveComparison().isEqualTo(expectedCommand);
        }

        @Test
        public void then_it_should_respond_with_the_new_team_identifier() {
            var expectedResponse = new MakeTeamResponse(TEAM_ID);
            assertThat(response.getBody()).usingRecursiveComparison().isEqualTo(expectedResponse);
        }

        @Test
        public void then_it_should_respond_with_status_code_OK() {
            assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        }

        private ResponseEntity<MakeTeamResponse> response;
        private MakeTeamResource SUT;
        private MakeTeamResourceFixture fixture;

        private final UUID TEAM_ID = UUID.fromString("06f1db66-163a-11ec-94ab-acde48001122");
    }

    @Nested
    class When_making_a_new_team_using_an_invalid_request
        extends ContextSpecification {

        @Override
        protected void establishContext() {

            SUT = new MakeTeamResourceFixture()
                .buildSUT();
        }

        @Override
        protected void because() {

            var invalidRequest = new MakeTeamRequest(null);
            response = SUT.makeTeam(4873, invalidRequest);
        }

        @Test
        public void then_it_should_respond_with_status_code_bad_request() {
            assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        }

        private ResponseEntity<MakeTeamResponse> response;
        private MakeTeamResource SUT;
    }

    @Nested
    class When_making_a_new_team_results_in_a_domain_violation
        extends ContextSpecification {

        @Override
        protected void establishContext() {

            SUT = new MakeTeamResourceFixture()
                .sutCanQuery(UUID.fromString("06f1db66-163a-11ec-94ab-acde48001122"))
                .sutCanQuery(Result.failure(new DomainViolation("Request does not meet domain criteria.")))
                .buildSUT();
        }

        @Override
        protected void because() {
            var request = new MakeTeamRequest("U13");
            response = SUT.makeTeam(4873, request);
        }

        @Test
        public void then_it_should_respond_with_status_code_bad_request() {
            assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        }

        private ResponseEntity<MakeTeamResponse> response;
        private MakeTeamResource SUT;
    }

    public static class MakeTeamResourceFixture {

        private final CommandHandler<MakeTeamCommand> makeTeamHandler;
        private final TeamRepository teamRepository;

        public MakeTeamResourceFixture() {
            makeTeamHandler = Mockito.mock(CommandHandler.class);
            teamRepository = Mockito.mock(TeamRepository.class);
        }

        public MakeTeamResourceFixture sutCanQuery(UUID teamId) {
            Mockito.when(teamRepository.getNextIdentity()).thenReturn(teamId);
            return this;
        }

        public MakeTeamResourceFixture sutCanQuery(Result result) {
            Mockito.when(makeTeamHandler.handle(Mockito.any())).thenReturn(result);
            return this;
        }

        public MakeTeamResource buildSUT() {
            return new MakeTeamResource(teamRepository, makeTeamHandler);
        }

        public MakeTeamCommand interceptDispatchedCommand() {
            var dispatchedCommandCaptor = ArgumentCaptor.forClass(MakeTeamCommand.class);
            Mockito.verify(makeTeamHandler).handle(dispatchedCommandCaptor.capture());
            return dispatchedCommandCaptor.getValue();
        }
    }
}
