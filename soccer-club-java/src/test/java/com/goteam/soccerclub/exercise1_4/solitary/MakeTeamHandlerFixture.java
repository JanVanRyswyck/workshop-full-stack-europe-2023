package com.goteam.soccerclub.exercise1_4.solitary;

import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domain.team.MakeTeamHandler;
import com.goteam.soccerclub.domain.team.Team;

import java.time.LocalDate;

public class MakeTeamHandlerFixture {

    // Step 1: uncomment these private fields
    //private final Clock clock;
    //private final ClubRepository clubRepository;
    //private final TeamRepository teamRepository;

    public MakeTeamHandlerFixture() {
        // Step 2: initialise the private fields with test doubles (e.g. `Mockito.mock(...)`).
    }

    public MakeTeamHandlerFixture sutCanQuery(Club club) {
        // Step 3: let the `ClubRepository` return the specified `Club` instance.
        // Tip: You can use `Mockito.anyInt()` to allow for flexible argument matching.
        return this;
    }

    public MakeTeamHandlerFixture sutCanQuery(LocalDate currentDate) {
        // Step 4: let the `Clock` return the specified current date.
        return this;
    }

    public MakeTeamHandler buildSUT() {
        // Step 5: build an instance of the `MakeTeamHandler` using the private fields.
        // Tip: Use a concrete instance of the `TeamFactory` instead of a test double.
        return null;
    }

    public Team interceptPersistedTeam() {
        // Step 6: the purpose of this method is to capture the instance of a `Team` when it's provided to the
        // `save` method of the `TeamRepository`. This captured `Team` object should then be returned.
        //
        // Tip: You can make use of an argument captor (e.g. `ArgumentCaptor.forClass(...)`).
        return null;
    }
}
