package com.goteam.soccerclub.exercise1_4.sociable;

import com.goteam.soccerclub.SoccerclubApplication;
import com.goteam.soccerclub.api.team.MakeTeamRequest;
import com.goteam.soccerclub.api.team.MakeTeamResponse;
import com.goteam.soccerclub.common.database.SoccerClubDatabase;
import com.goteam.soccerclub.common.database.SociableTestConfigurationInitializer;
import com.goteam.soccerclub.common.fakes.FakeClock;
import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domainports.Clock;
import com.goteam.soccerclub.domainports.club.ClubRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(
    initializers = { SociableTestConfigurationInitializer.class },
    classes = { MakeTeamTests.Configuration.class, SoccerClubDatabase.class })
@Import(SoccerclubApplication.class)
public class MakeTeamTests {

    private static final String UUID_PATTERN = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";

    private final TestRestTemplate testRestTemplate;
    private final SoccerClubDatabase soccerClubDatabase;
    private final ClubRepository clubRepository;

    @Autowired
    public MakeTeamTests(TestRestTemplate testRestTemplate,
                         SoccerClubDatabase soccerClubDatabase,
                         ClubRepository clubRepository) {

        this.testRestTemplate = testRestTemplate;
        this.soccerClubDatabase = soccerClubDatabase;
        this.clubRepository = clubRepository;
    }

    @BeforeEach
    public void setUp() {
        soccerClubDatabase.initialise()
            .enforceForeignKeys();
    }

    @AfterEach
    public void tearDown() {
        soccerClubDatabase.cleanup();
    }

    @Test
    public void Should_receive_an_OK_response_together_with_the_identifier_of_the_newly_made_team() {

        clubRepository.save(new Club(4873, "Bayern München"));

        var request = new MakeTeamRequest("Team A");

        ResponseEntity<MakeTeamResponse> response = testRestTemplate.exchange(
            "/api/clubs/{clubNumber}/teams",
            HttpMethod.POST,
            new HttpEntity<>(request),
            MakeTeamResponse.class,
            4873
        );

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        var body = response.getBody();
        assertThat(body).isNotNull();
        assertThat(body.getTeamId()).isNotNull();
        assertThat(body.getTeamId().toString()).matches(UUID_PATTERN);
    }

    static class Configuration {

        @Bean
        public Clock clock() {
            return new FakeClock(LocalDate.of(2021, 6, 15));
        }
    }
}