package com.goteam.soccerclub.exercise1_4.solitary;

import com.goteam.soccerclub.common.ContextSpecification;
import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domain.team.MakeTeamHandler;
import com.goteam.soccerclub.domainports.Result;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

//
// For this test suite we're going to apply the "Arrange, Act, Assert per test class" approach.
//
public class MakeTeamHandlerTests {

    // The happy path scenario.
    // A new `Team` should be instantiated by the `TeamFactory`. After that, the newly created `Team` should be saved
    // in the repository.
    @Nested
    class When_making_a_new_team
        extends ContextSpecification {

        @Override
        protected void establishContext() {

            // Arrange stage
            // Step 1: `create a `Club` using the corresponding test data builder (e.g. `Example.club()...`).`
            club = null;

            // Step 2: create an instance of the `MakeTeamHandlerFixture`.
            fixture = null;

            // Step 3: create an instance of the Subject Under Test by using the fixture object.
            // Also specify the necessary indirect inputs using the `sutCanQuery` method (e.g. a `Club` object, a
            // `LocalDate` representing the current date).
            SUT = null;
        }

        @Override
        protected void because() {

            // Act stage
            // Execute the `handle` method on the `MakeTeamHandler` instance created in `establishContext`. You can
            // store the result of the operation in the `result` member variable.
        }
        @Test
        public void then_it_should_persist_a_new_team() {

            // Assert stage - part 1
            // Observe that a `Team` instance is saved by the `GameRepository`.
            // Use the `interceptPersistedTeam` method of the fixture object to capture the indirect output (e.g. the
            // persisted `Team`).
            //
            // Tip: Use the `TeamBuilder` (e.g. `Example.team()...`) to create an expected `Team` instance and use the
            // `usingRecursiveComparison` to verify the saved `Team`.
        }

        @Test
        public void then_it_should_indicate_a_successful_operation() {

            // Assert stage - part 2
            // Observe whether the return value indicates a successful operation.
        }

        private Club club;
        private MakeTeamHandlerFixture fixture;
        private Result result;
        private MakeTeamHandler SUT;
    }

    // Edge case #1
    // When the specified club doesn't exist, then the result of the `handle` method should indicate a failed operation
    // by returning a domain violation.
    @Nested
    class When_making_a_new_team_for_an_unknown_club
        extends ContextSpecification {

        @Override
        protected void establishContext() {

            // Arrange stage
            // Create an instance of the Subject Under Test by using the `MakeTeamHandlerFixture`. Specify a null
            // reference to be returned as the indirect input for a `Club`.
            SUT = null;
        }

        @Override
        protected void because() {

            // Act stage
            // Execute the `handle` method on the `MakeTeamHandler` instance created in `establishContext`.
        }

        @Test
        public void then_it_should_indicate_a_failed_operation() {

            // Assert stage
            // Create an instance of the expected result (MakeTeamHandler.UNKNOWN_CLUB_VIOLATION), and use
            // `usingRecursiveComparison` to verify the actual result.
        }

        private Result result;
        private MakeTeamHandler SUT;
    }

    // Edge case #2
    // When a domain violation is issued by the `TeamFactory` while making a new team, then the result of the
    // `handle` method should indicate a failed operation by returning that domain violation.
    @Nested
    class When_making_a_new_team_results_in_a_failure
        extends ContextSpecification {

        @Override
        protected void establishContext() {

            // Arrange stage
            // Create an instance of the Subject Under Test by using the `MakeTeamHandlerFixture`. Specify the necessary
            // indirect inputs using the `sutCanQuery` method (e.g. a `Club` object, a `LocalDate` representing the
            // current date).
            SUT = null;
        }

        @Override
        protected void because() {

            // Act stage
            // Execute the `handle` method on the `MakeTeamHandler` instance created in `establishContext`.
        }

        @Test
        public void then_it_should_indicate_a_failed_operation() {

            // Assert stage
            // Create an instance of the expected result (TeamFactory.TEAM_COMPOSITION_PERIOD_VIOLATION), and use
            // `usingRecursiveComparison` to verify the actual result.
        }

        private Result result;
        private MakeTeamHandler SUT;
    }
}
