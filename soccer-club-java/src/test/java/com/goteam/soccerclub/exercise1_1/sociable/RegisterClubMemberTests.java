package com.goteam.soccerclub.exercise1_1.sociable;

import com.goteam.soccerclub.SoccerclubApplication;
import com.goteam.soccerclub.api.clubmember.RegisterClubMemberRequest;
import com.goteam.soccerclub.api.clubmember.RegisterClubMemberResponse;
import com.goteam.soccerclub.common.database.SoccerClubDatabase;
import com.goteam.soccerclub.common.database.SociableTestConfigurationInitializer;
import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domainports.club.ClubRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = { SociableTestConfigurationInitializer.class }, classes = { SoccerClubDatabase.class })
@Import(SoccerclubApplication.class)
public class RegisterClubMemberTests {

    private static final String UUID_PATTERN = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";

    private final TestRestTemplate testRestTemplate;
    private final SoccerClubDatabase soccerClubDatabase;
    private final ClubRepository clubRepository;

    @Autowired
    public RegisterClubMemberTests(TestRestTemplate testRestTemplate,
                                   SoccerClubDatabase soccerClubDatabase,
                                   ClubRepository clubRepository) {

        this.testRestTemplate = testRestTemplate;
        this.soccerClubDatabase = soccerClubDatabase;
        this.clubRepository = clubRepository;
    }

    @BeforeEach
    public void setUp() {
        soccerClubDatabase.initialise()
            .enforceForeignKeys();
    }

    @AfterEach
    public void tearDown() {
        soccerClubDatabase.cleanup();
    }

    @Test
    public void Should_receive_an_OK_response_together_with_the_identifier_of_the_newly_registered_club_member() {

        clubRepository.save(new Club(6354, "FC Barcelona"));

        var request = new RegisterClubMemberRequest("Andres", "Iniesta", "andres@iniesta.es",
            LocalDate.of(1984, 5, 11), "84051135476");

        ResponseEntity<RegisterClubMemberResponse> response = testRestTemplate.exchange(
            "/api/clubs/{clubNumber}/members",
            HttpMethod.POST,
            new HttpEntity<>(request),
            RegisterClubMemberResponse.class,
            6354);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        var body = response.getBody();
        assertThat(body).isNotNull();
        assertThat(body.getClubMemberId()).isNotNull();
        assertThat(body.getClubMemberId().toString()).matches(UUID_PATTERN);
    }
}