package com.goteam.soccerclub.exercise1_1.solitary;

import com.goteam.soccerclub.domain.clubmember.ClubMemberFactory;
import com.goteam.soccerclub.domain.clubmember.RegisterClubMemberHandler;
import com.goteam.soccerclub.domainports.club.ClubRepository;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberNotifier;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class RegisterClubMemberHandlerTests {

    private ClubRepository clubRepository;
    private ClubMemberRepository clubMemberRepository;
    private ClubMemberNotifier clubMemberNotifier;
    private ClubMemberFactory clubMemberFactory;
    private RegisterClubMemberHandler SUT;

    @BeforeEach
    public void setUp() {
        clubRepository = Mockito.mock(ClubRepository.class);
        clubMemberRepository = Mockito.mock(ClubMemberRepository.class);
        clubMemberNotifier = Mockito.mock(ClubMemberNotifier.class);

        // !! START HERE !!
        // Do you want to use a test double for the "ClubMemberFactory"? Or would you rather provide the handler a
        // concrete instance of "ClubMemberFactory"? Just relax. There's not really a "right" answer. Either approach
        // has its advantages and disadvantages.
        // Be mindful about your choice while implementing the next couple of tests.
        clubMemberFactory = null;

        SUT = new RegisterClubMemberHandler(clubRepository, clubMemberRepository, clubMemberFactory, clubMemberNotifier);
    }

    @Test
    public void registerClubMember_shouldPersistNewClubMember() {

        // The happy path scenario - part 1
        // So far, we've been writing "State Verification" tests. For this test case, we're going to give "Behaviour Verification"
        // a try. The purpose of this test is to verify that a newly created `ClubMember` is saved by the `ClubMemberRepository`.
        //
        // Tip: An instance of a `Club` can be retrieved using the `ClubRepository`.
        // Tip: An instance of a `ClubMember` can be created using the `ClubMemberFactory`.

    }

    @Test
    public void registerClubMember_shouldSendPaymentRequestForMembershipFee() {

        // The happy path scenario - part 2
        // We're going to write yet another "Behaviour Verification" test. The purpose of this test is to verify that
        // a payment request is sent to the new club member using the `ClubMemberNotifier`.

    }

    @Test
    public void registerClubMember_shouldIndicateSuccess() {

        // The happy path scenario - part 3
        // This "State Verification" test verifies whether the return value indicates a successful operation.

    }

    @Test
    public void registerClubMember_unknownClub_shouldIndicateFailure() {

        // Edge case #1
        // When the specified club doesn't exist, then the result of the `handle` method should indicate a failed
        // operation by returning a domain violation.
        //
        // Tip: You can use the `UNKNOWN_CLUB_VIOLATION_MESSAGE` static field as the domain violation message.

    }

    @Test
    public void registerClubMember_clubMemberAlreadyRegistered_shouldIndicateFailure() {

        // Edge case #2
        // When the club member has already been registered, then the result of the `handle` method should indicate a
        // failed operation by returning a domain violation.
        //
        // Tip: An instance of a `ClubMember` can be retrieved using the `ClubMemberRepository`.
        // Tip: You can use the `CLUB_MEMBER_ALREADY_REGISTERED_VIOLATION_MESSAGE` static field as the domain violation message.

    }

    @Test
    public void registerClubMember_registrationFailure_shouldIndicateFailure() {

        // Edge case #3
        // When the `ClubMemberFactory` returns a domain violation (the right value of the pair), then the result of the
        // `handle` method should indicate a failed operation by returning the domain violation of the factory.

    }
}
