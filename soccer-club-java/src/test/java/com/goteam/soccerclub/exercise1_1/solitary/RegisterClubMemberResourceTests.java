package com.goteam.soccerclub.exercise1_1.solitary;

import com.goteam.soccerclub.api.clubmember.RegisterClubMemberResource;
import com.goteam.soccerclub.api.clubmember.RegisterClubMemberRequest;
import com.goteam.soccerclub.api.clubmember.RegisterClubMemberResponse;
import com.goteam.soccerclub.domainports.CommandHandler;
import com.goteam.soccerclub.domainports.DomainViolation;
import com.goteam.soccerclub.domainports.Result;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberRepository;
import com.goteam.soccerclub.domainports.clubmember.RegisterClubMemberCommand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class RegisterClubMemberResourceTests {

    private ClubMemberRepository clubMemberRepository;
    private CommandHandler<RegisterClubMemberCommand> registerClubMemberHandler;
    private RegisterClubMemberResource SUT;

    @BeforeEach
    public void setUp() {
        registerClubMemberHandler = Mockito.mock(CommandHandler.class);
        clubMemberRepository = Mockito.mock(ClubMemberRepository.class);

        SUT = new RegisterClubMemberResource(clubMemberRepository, registerClubMemberHandler);
    }

    @Test
    public void registerClubMember_validRequest_shouldDispatchCommandToDomain() {

        var clubMemberId = UUID.fromString("c14cdb79-39ef-4248-95d4-209eb6d9999c");
        Mockito.when(clubMemberRepository.getNextIdentity()).thenReturn(clubMemberId);
        Mockito.when(registerClubMemberHandler.handle(Mockito.any())).thenReturn(Result.success());

        var request = new RegisterClubMemberRequest("Xavi", "Hernández", "xavi@hernandez.es",
            LocalDate.of(1980, 1, 25), "80012546532");
        SUT.registerClubMember(5342, request);

        var dispatchedCommandCaptor = ArgumentCaptor.forClass(RegisterClubMemberCommand.class);
        Mockito.verify(registerClubMemberHandler).handle(dispatchedCommandCaptor.capture());

        var expectedCommand = new RegisterClubMemberCommand(5342, clubMemberId, "Xavi", "Hernández",
            "xavi@hernandez.es", LocalDate.of(1980, 1, 25), "80012546532");

        assertThat(dispatchedCommandCaptor.getValue()).usingRecursiveComparison().isEqualTo(expectedCommand);
    }

    @Test
    public void registerClubMember_validRequest_shouldRespondWithStatusCodeOK() {

        var clubMemberId = UUID.fromString("c14cdb79-39ef-4248-95d4-209eb6d9999c");
        Mockito.when(clubMemberRepository.getNextIdentity()).thenReturn(clubMemberId);
        Mockito.when(registerClubMemberHandler.handle(Mockito.any())).thenReturn(Result.success());

        var request = new RegisterClubMemberRequest("Xavi", "Hernández", "xavi@hernandez.es",
            LocalDate.of(1980, 1, 25), "80012546532");

        var response = SUT.registerClubMember(5342, request);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void registerClubMember_validRequest_shouldRespondWithNewClubMemberIdentifier() {

        var clubMemberId = UUID.fromString("c14cdb79-39ef-4248-95d4-209eb6d9999c");
        Mockito.when(clubMemberRepository.getNextIdentity()).thenReturn(clubMemberId);
        Mockito.when(registerClubMemberHandler.handle(Mockito.any())).thenReturn(Result.success());

        var request = new RegisterClubMemberRequest("Xavi", "Hernández", "xavi@hernandez.es",
            LocalDate.of(1980, 1, 25),"80012546532");

        var response = SUT.registerClubMember(5342, request);

        var expectedResponse = new RegisterClubMemberResponse(clubMemberId);
        assertThat(response.getBody()).usingRecursiveComparison().isEqualTo(expectedResponse);
    }

    @Test
    public void registerClubMember_invalidRequest_shouldRespondWithStatusCodeBadRequest() {

        var invalidRequest = new RegisterClubMemberRequest(null, null, null, null, null);
        var response = SUT.registerClubMember(5342, invalidRequest);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    } 
    
    @Test
    public void registerClubMember_requestDoesNotMeetDomainCriteria_shouldRespondWithStatusCodeBadRequest() {

        Mockito.when(clubMemberRepository.getNextIdentity()).thenReturn(UUID.randomUUID());
        Mockito.when(registerClubMemberHandler.handle(Mockito.any()))
            .thenReturn(Result.failure(new DomainViolation("Request does not meet domain criteria")));

        var requestToBeRejected = new RegisterClubMemberRequest("Arwen", "Too Young", "arwen@tooyoung.com",
            LocalDate.of(1980, 1, 25), "19081283364");

        var response = SUT.registerClubMember(5342, requestToBeRejected);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    } 
}
