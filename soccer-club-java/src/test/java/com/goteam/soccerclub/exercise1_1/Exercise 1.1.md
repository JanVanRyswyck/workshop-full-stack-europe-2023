# Exercise 1.1

![](Exercise_1.1.png)

## 1. Solitary tests

### 1.1 ClubMemberFactory

Open the files `ClubMemberFactory.java` and `ClubMemberFactoryTests.java`. Complete the implementation of the provided 
test cases using a Test-First approach.

1. Implement only a single test case at a time. Make sure that the test fails.
   (Tip: a compilation error due to non-existing code invoked by a test method also classifies as a failing test)
2. Provide the least amount of code to make the test pass.
3. Refactor, refactor, refactor!
4. Repeat these steps until all the tests pass.

### 1.2 RegisterClubMemberHandler

Open the files `RegisterClubMemberHandler.java` and `RegisterClubMemberHandlerTests.java`.

1. Start in the `setUp` method of `RegisterClubMemberHandlerTests.java`. 
2. Complete the implementation of the provided test cases using a Test-First approach. Remember to only implement a 
   single test case at a time.

## 2. Sociable tests

Open the file `RegisterClubMemberTests.java`. There's a single Sociable test that calls the API endpoint for registering
a new club member. The test verifies the status code of the HTTP response and also the returned identifier of the newly
created club member. Go ahead and execute this test. At this point, the test should be green as you implemented all the 
missing pieces in the previous steps.

There's nothing that should be done for this Sociable test as it has already been completely implemented. You can proceed
to the next step.

## 3. Open questions

* Which kind of tests gave you the fastest feedback? The Solitary tests or the single Sociable test?
* Can you make an observation regarding the ratio between Solitary tests and Sociable tests?
* Did you choose a concrete instance of the "ClubMemberFactory"? Or did you use a test double? What would be the 
  implications of making a different choice?

