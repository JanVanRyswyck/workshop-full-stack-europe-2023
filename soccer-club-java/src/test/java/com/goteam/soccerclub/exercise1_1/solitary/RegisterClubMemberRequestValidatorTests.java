package com.goteam.soccerclub.exercise1_1.solitary;

import com.goteam.soccerclub.api.clubmember.RegisterClubMemberRequest;
import com.goteam.soccerclub.api.clubmember.RegisterClubMemberRequestValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

public class RegisterClubMemberRequestValidatorTests {

    @Test
    public void validate_validRequest_shouldIndicatePositiveResult() {

        var validRequest = new RegisterClubMemberRequest("Raheem", "Sterling", "raheem@sterling.co.uk",
            LocalDate.of(1994, 12, 8), "94120874634");

        var isValid = new RegisterClubMemberRequestValidator().validate(validRequest);
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = { "", " " })
    public void validate_requestWithoutFirstName_shouldIndicateNegativeResult(String invalidFirstName) {

        var invalidRequest = new RegisterClubMemberRequest(invalidFirstName, "Sterling", "raheem@sterling.co.uk",
            LocalDate.of(1994, 12, 8), "94120874634");

        var isValid = new RegisterClubMemberRequestValidator().validate(invalidRequest);
        assertThat(isValid).isFalse();
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = { "", " " })
    public void validate_requestWithoutLastName_shouldIndicateNegativeResult(String invalidLastName) {

        var invalidRequest = new RegisterClubMemberRequest("Raheem", invalidLastName, "raheem@sterling.co.uk",
            LocalDate.of(1994, 12, 8), "94120874634");

        var isValid = new RegisterClubMemberRequestValidator().validate(invalidRequest);
        assertThat(isValid).isFalse();
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = { "", " " })
    public void validate_requestWithoutEmail_shouldIndicateNegativeResult(String invalidEmail) {

        var invalidRequest = new RegisterClubMemberRequest("Raheem", "Sterling", invalidEmail,
            LocalDate.of(1994, 12, 8), "94120874634");

        var isValid = new RegisterClubMemberRequestValidator().validate(invalidRequest);
        assertThat(isValid).isFalse();
    }

    @Test
    public void validate_requestWithoutBirthDate_shouldIndicateNegativeResult() {

        var invalidRequest = new RegisterClubMemberRequest("Raheem", "Sterling", "raheem@sterling.co.uk",
            null, "94120874634");

        var isValid = new RegisterClubMemberRequestValidator().validate(invalidRequest);
        assertThat(isValid).isFalse();
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = { "", " " })
    public void validate_requestWithoutNationalIdentificationNumber_shouldIndicateNegativeResult(String invalidNationalIdentificationNumber) {

        var invalidRequest = new RegisterClubMemberRequest("Raheem", "Sterling", "raheem@sterling.co.uk",
            LocalDate.of(1994, 12, 8), invalidNationalIdentificationNumber);

        var isValid = new RegisterClubMemberRequestValidator().validate(invalidRequest);
        assertThat(isValid).isFalse();
    }

    @ParameterizedTest
    @ValueSource(strings = {
        "4120874634",       // Less than 11 digits
        "941208746345",     // More than 11 digits
        "94120E74634",      // Only digits are allowed
        "95120874634",      // Year for date of birth does not match
        "94130874634",      // Month for date of birth does not match
        "94120974634"       // Day for date of birth does not match
    })
    public void validate_requestWithInvalidNationalIdentificationNumber_shouldIndicateNegativeResult(String invalidNationalIdentificationNumber) {

        var invalidRequest = new RegisterClubMemberRequest("Raheem", "Sterling", "raheem@sterling.co.uk",
            LocalDate.of(1994, 12, 8), invalidNationalIdentificationNumber);

        var isValid = new RegisterClubMemberRequestValidator().validate(invalidRequest);
        assertThat(isValid).isFalse();
    }
}
