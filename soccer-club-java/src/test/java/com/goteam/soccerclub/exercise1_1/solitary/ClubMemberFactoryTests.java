package com.goteam.soccerclub.exercise1_1.solitary;

import com.goteam.soccerclub.domain.clubmember.ClubMemberFactory;
import com.goteam.soccerclub.domainports.Clock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class ClubMemberFactoryTests {

    private ClubMemberFactory SUT;
    private Clock clock;

    @BeforeEach
    public void setUp() {
        clock = Mockito.mock(Clock.class);
        SUT = new ClubMemberFactory(clock);
    }

    @Test
    public void createClubMember_shouldCreateNewClubMember() {

        // The happy path scenario.
        // The `createClubMember` method should return a pair, with a newly created `ClubMember` on the left and
        // a null reference on the right.

    }

    @Test
    public void createClubMember_youngerThanMinimumAge_shouldIndicateFailure() {

        // The soccer association imposes a general rule on clubs: a member should not be younger than five years old.
        // A child of only four years old or younger is not allowed to play soccer in any competition.
        // As a result, when the specified birthdate indicates that the person in question is not at least five years
        // old, the `createClubMember` method should return a pair with a null reference on the left, and a domain
        // violation on the right.
        //
        // Tip: The `Clock` can be used to retrieve the current date.
        // Tip: The `MINIMUM_AGE_VIOLATION_MESSAGE` static field can be used as the domain violation message.

    }
}
