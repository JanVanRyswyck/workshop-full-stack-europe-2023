package com.goteam.soccerclub.exercise1_2.solitary;

import com.goteam.soccerclub.api.clubmember.AssignCertificationRequest;
import com.goteam.soccerclub.api.clubmember.AssignCertificationRequestValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

public class AssignCertificationRequestValidatorTests {

    @Test
    public void validate_validRequest_shouldIndicatePositiveResult() {

        var validRequest = new AssignCertificationRequest("UEFA-A", LocalDate.of(2021, 7, 27));
        var isValid = new AssignCertificationRequestValidator().validate(validRequest);
        assertThat(isValid).isTrue();
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = { "", " " })
    public void validate_requestWithoutCertificateCode_shouldIndicateNegativeResult(String invalidCertificateCode) {

        var invalidRequest = new AssignCertificationRequest(invalidCertificateCode, LocalDate.of(2021, 7, 27));
        var isValid = new AssignCertificationRequestValidator().validate(invalidRequest);
        assertThat(isValid).isFalse();
    }

    @Test
    public void validate_requestWithoutIssueDate_shouldIndicateNegativeResult() {

        var invalidRequest = new AssignCertificationRequest("UEFA-A", null);
        var isValid = new AssignCertificationRequestValidator().validate(invalidRequest);
        assertThat(isValid).isFalse();
    }
}