package com.goteam.soccerclub.exercise1_2.solitary;

import com.goteam.soccerclub.common.ContextSpecification;
import com.goteam.soccerclub.domain.clubmember.AssignCertificationHandler;
import com.goteam.soccerclub.domain.clubmember.ClubMember;
import com.goteam.soccerclub.domainports.Result;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberNotifier;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberRepository;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

//
// For this test suite we're going to apply the  "Arrange, Act, Assert per test class" approach.
//
public class AssignCertificationHandlerTests {

    // The happy path scenario.
    // When assigning a certification, a new `Certification` should be added to the `ClubMember`. After that, the
    // `ClubMember` instance should be saved in the repository. We should also send a message to the club member
    // congratulating her/him for this amazing accomplishment. This implies that we're going to make multiple
    // observations.
    @Nested
    class When_assigning_a_certification_to_a_club_member
        extends ContextSpecification {

        @Override
        public void establishContext() {

            // Arrange stage
            // Create an instance of a `AssignCertificationHandler` (SUT) and everything that is necessary for executing
            // the scenario.
            //
            // Tip: An instance of a `ClubMember` can be retrieved using the `ClubRepository`.
        }

        @Override
        protected void because() {

            // Act stage
            // Execute the `handle` method on the `AssignCertificationHandler` instance created in `establishContext`.
        }

        @Test
        public void then_the_club_member_should_have_a_new_certification() {

            // Assert stage - part 1
            // Observe that a new `Certification` has been added to the `certifications` collection of the `ClubMember`
            // instance.
        }

        @Test
        public void then_the_club_member_should_be_persisted() {

            // Assert stage - part 2
            // Observe that the `ClubMember` instance is saved by the `ClubMemberRepository`.
        }
        
        @Test
        public void then_a_congratulatory_message_should_be_send_to_the_club_member() {

            // Assert stage - part 3
            // Observe that a congratulatory message is sent to the club member using the `ClubMemberNotifier`.
        } 

        @Test
        public void then_it_should_indicate_a_successful_operation() {
            // Assert stage - part 4
            // Observe whether the return value indicates a successful operation.
        }

        private ClubMember clubMember;
        private Result result;
        
        private ClubMemberRepository clubMemberRepository;
        private ClubMemberNotifier clubMemberNotifier;
        private AssignCertificationHandler SUT;
    }

    // Edge case #1
    // When the specified club member doesn't exist, then the result of the `Handle` method should indicate a failed
    // operation by returning a domain violation.
    @Nested
    class When_assigning_a_certification_to_an_unknown_club_member
        extends ContextSpecification {

        @Override
        public void establishContext() {

            // Arrange stage
            // Create an instance of a `AssignCertificationHandler` (SUT) and everything that is necessary for executing
            // the scenario.
        }

        @Override
        protected void because() {

            // Act stage
            // Execute the `handle` method on the `AssignCertificationHandler` instance created in `establishContext`.
        }

        @Test
        public void then_it_should_indicate_a_failed_operation() {

            // Assert stage - part 1
            // Observe whether the return value indicates a failed operation.
        }

        @Test
        public void then_the_result_should_specify_a_corresponding_domain_violation() {

            // Assert stage - part 2
            // Observe whether the return value contains the `UNKNOWN_CLUB_MEMBER_VIOLATION` domain violation.
        }

        private Result result;
        private AssignCertificationHandler SUT;
    }

    // Edge case #2
    // When assigning a certification to a club member ends up with a domain violation, then the result of the
    // `Handle` method should indicate a failed operation by returning that domain violation.
    @Nested
    class When_assigning_a_certification_to_a_club_member_results_in_a_failure
        extends ContextSpecification {

        @Override
        public void establishContext() {

            // Arrange stage
            // Create an instance of a `AssignCertificationHandler` (SUT) and everything that is necessary for executing
            // the scenario.
            //
            // Tip: The `Clock` to retrieve the current date.
            // Tip: The `IterableUtils` of the Apache Commons Collections library has a number of useful methods
            // to deal with `Iterable` collections. (`import org.apache.commons.collections4.IterableUtils`)
        }

        @Override
        protected void because() {

            // Act stage
            // Execute the `handle` method on the `AssignCertificationHandler` instance created in `establishContext`.
        }

        @Test
        public void then_it_should_indicate_a_failed_operation() {

            // Assert stage - part 1
            // Observe whether the return value indicates a failed operation.
        }

        @Test
        public void then_the_result_should_specify_a_corresponding_domain_violation() {

            // Assert stage - part 2
            // Observe whether the return value contains the `INVALID_ISSUE_DATE_VIOLATION` domain violation.
        }

        private Result result;
        private AssignCertificationHandler SUT;
    }
}
