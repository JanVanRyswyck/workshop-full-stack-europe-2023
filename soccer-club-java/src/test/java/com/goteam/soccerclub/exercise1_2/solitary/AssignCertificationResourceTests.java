package com.goteam.soccerclub.exercise1_2.solitary;

import com.goteam.soccerclub.api.clubmember.AssignCertificationRequest;
import com.goteam.soccerclub.api.clubmember.AssignCertificationResource;
import com.goteam.soccerclub.domainports.CommandHandler;
import com.goteam.soccerclub.domainports.DomainViolation;
import com.goteam.soccerclub.domainports.Result;
import com.goteam.soccerclub.domainports.clubmember.AssignCertificationCommand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class AssignCertificationResourceTests {

    private CommandHandler<AssignCertificationCommand> assignCertificationHandler;
    private AssignCertificationResource SUT;

    @BeforeEach
    public void setUp() {
        assignCertificationHandler = Mockito.mock(CommandHandler.class);
        SUT = new AssignCertificationResource(assignCertificationHandler);
    }

    @Test
    public void assignCertification_validRequest_shouldDispatchCommandToDomain() {

        Mockito.when(assignCertificationHandler.handle(Mockito.any())).thenReturn(Result.success());

        var request = new AssignCertificationRequest("INITIATOR", LocalDate.of(2021, 7, 27));
        SUT.assignCertification(5342, UUID.fromString("75595c84-ef7d-11eb-be6e-acde48001122"), request);

        var dispatchedCommandCaptor = ArgumentCaptor.forClass(AssignCertificationCommand.class);
        Mockito.verify(assignCertificationHandler).handle(dispatchedCommandCaptor.capture());

        var expectedCommand = new AssignCertificationCommand(5342, UUID.fromString("75595c84-ef7d-11eb-be6e-acde48001122"),
            "INITIATOR", LocalDate.of(2021, 7, 27));

        assertThat(dispatchedCommandCaptor.getValue()).usingRecursiveComparison().isEqualTo(expectedCommand);
    }

    @Test
    public void assignCertification_validRequest_shouldRespondWithStatusCodeOK() {

        Mockito.when(assignCertificationHandler.handle(Mockito.any())).thenReturn(Result.success());

        var request = new AssignCertificationRequest("INITIATOR", LocalDate.of(2021, 7, 27));
        
        var response = SUT.assignCertification(
            5342, UUID.fromString("75595c84-ef7d-11eb-be6e-acde48001122"), request);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
    
    @Test
    public void assignCertification_invalidRequest_shouldRespondWithStatusCodeBadRequest() {

        var invalidRequest = new AssignCertificationRequest(null, null);

        var response = SUT.assignCertification(
            5342, UUID.fromString("75595c84-ef7d-11eb-be6e-acde48001122"), invalidRequest);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    } 
    
    @Test
    public void assignCertification_requestDoesNotMeetDomainCriteria_shouldRespondWithStatusCodeBadRequest() {

        Mockito.when(assignCertificationHandler.handle(Mockito.any()))
            .thenReturn(Result.failure(new DomainViolation("Request does not meet domain criteria")));

        var requestToBeRejected = new AssignCertificationRequest(
            "START2COACH", LocalDate.of(2021, 7, 27));

        var response = SUT.assignCertification(
            5342, UUID.fromString("75595c84-ef7d-11eb-be6e-acde48001122"), requestToBeRejected);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    } 
}
