package com.goteam.soccerclub.exercise1_2.solitary;

import com.goteam.soccerclub.common.ContextSpecification;
import com.goteam.soccerclub.domain.clubmember.ClubMember;
import com.goteam.soccerclub.domainports.Clock;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

//
// For this test suite we're going to apply the  "Arrange, Act, Assert per test class" approach.
//
public class AssignCertificationToClubMemberTests {

    // The happy path scenario.
    // When assigning a certification, then it should be added to the list of certifications.
    @Nested
    class When_assigning_a_certification
        extends ContextSpecification {

        @Override
        public void establishContext() {

            // Arrange stage
            // Create an instance of a `ClubMember` (SUT) and everything that is necessary for executing the scenario.
        }

        @Override
        protected void because() {

            // Act stage
            // Execute the `assignCertification` method on the `ClubMember` instance created in `establishContext`.
        }

        @Test
        public void then_the_club_member_should_have_a_new_certification() {

            // Assert stage
            // Observe that a new `Certification` has been added to the `certifications` list.
        }

        private Clock clock;
        private ClubMember SUT;
    }

    // Edge case #1
    // Assigning a certification with the same code should be an idempotent operation; meaning that there should be
    // only a single certification in the list. The first assigned certification should be kept.
    @Nested
    class When_assigning_the_same_certification_multiple_times
        extends ContextSpecification {

        @Override
        public void establishContext() {

            // Arrange stage
            // Create an instance of a `ClubMember` (SUT) and everything that is necessary for executing the scenario.
        }

        @Override
        protected void because() {

            // Act stage
            // Execute the `assignCertification` method on the `ClubMember` instance.
        }

        @Test
        public void then_the_club_member_should_only_have_the_first_assigned_certification() {

            // Assert stage
            // Observe that there's only a single `Certification` in the `certifications` list. This should be the
            // certification that got assigned first.
        }

        private Clock clock;
        private ClubMember SUT;
    }

    // Edge case #2
    // Assigning a certification with an issue date in the future is not allowed. In that case, a domain violation
    // should be issued and no new certification should be added to the list. This implies that we're going to make
    // two observations.
    @Nested
    class When_assigning_a_certification_with_an_issue_date_in_the_future
        extends ContextSpecification {

        @Override
        public void establishContext() {

            // Arrange stage
            // Create an instance of a `ClubMember` (SUT) and everything that is necessary for executing the scenario.
            //
            // Tip: The `Clock` to retrieve the current date.
        }

        @Override
        protected void because() {

            // Act stage
            // Execute the `assignCertification` method on the `ClubMember` instance, specifying a date that comes after
            // the current date specified in `establishContext`.
        }

        @Test
        public void then_it_should_result_in_a_domain_violation() {

            // Assert stage - part 1
            // Observe whether the `INVALID_ISSUE_DATE_VIOLATION` has been added to the `domainViolations` list.
        }

        @Test
        public void then_no_certification_should_be_assigned() {

            // Assert stage - part 2
            // Observe that there's no new `Certification` in the `certifications` list.
        }

        private Clock clock;
        private ClubMember SUT;
    }
}
