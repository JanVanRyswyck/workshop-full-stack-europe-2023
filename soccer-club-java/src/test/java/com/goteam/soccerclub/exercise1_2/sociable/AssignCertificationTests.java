package com.goteam.soccerclub.exercise1_2.sociable;

import com.goteam.soccerclub.SoccerclubApplication;
import com.goteam.soccerclub.api.clubmember.AssignCertificationRequest;
import com.goteam.soccerclub.common.database.SoccerClubDatabase;
import com.goteam.soccerclub.common.database.SociableTestConfigurationInitializer;
import com.goteam.soccerclub.domain.club.Club;
import com.goteam.soccerclub.domain.clubmember.ClubMember;
import com.goteam.soccerclub.domainports.club.ClubRepository;
import com.goteam.soccerclub.domainports.clubmember.ClubMemberRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = { SociableTestConfigurationInitializer.class }, classes = { SoccerClubDatabase.class })
@Import(SoccerclubApplication.class)
public class AssignCertificationTests {

    private final TestRestTemplate testRestTemplate;
    private final SoccerClubDatabase soccerClubDatabase;
    private final ClubRepository clubRepository;
    private final ClubMemberRepository clubMemberRepository;

    @Autowired
    public AssignCertificationTests(TestRestTemplate testRestTemplate,
                                    SoccerClubDatabase soccerClubDatabase,
                                    ClubRepository clubRepository,
                                    ClubMemberRepository clubMemberRepository) {

        this.testRestTemplate = testRestTemplate;
        this.soccerClubDatabase = soccerClubDatabase;
        this.clubRepository = clubRepository;
        this.clubMemberRepository = clubMemberRepository;
    }

    @BeforeEach
    public void setUp() {
        soccerClubDatabase.initialise()
            .enforceForeignKeys();
    }

    @AfterEach
    public void tearDown() {
        soccerClubDatabase.cleanup();
    }

    @Test
    public void Should_receive_an_OK_response() {

        var club = new Club(6354, "FC Barcelona");
        clubRepository.save(club);

        clubMemberRepository.save(new ClubMember(UUID.fromString("9fac088e-ef13-11eb-9d46-acde48001122"), club,
            "Andres", "Iniesta", "andres@iniesta.es", LocalDate.of(1984, 5, 11),
            "84051135476"));

        var request = new AssignCertificationRequest("UEFA-B", LocalDate.of(2021, 7, 27));

        ResponseEntity<Void> response = testRestTemplate.exchange(
            "/api/clubs/{clubNumber}/members/{clubMemberId}/certifications",
            HttpMethod.PUT,
            new HttpEntity<>(request),
            Void.class,
            6354, "9fac088e-ef13-11eb-9d46-acde48001122");

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}