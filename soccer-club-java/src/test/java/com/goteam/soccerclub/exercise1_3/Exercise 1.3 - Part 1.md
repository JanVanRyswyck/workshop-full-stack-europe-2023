# Exercise 1.3 - Part 1

For this exercise we're going to implement a couple of test data builders for domain objects.

## 1. ClubBuilder

1. Open the file `ClubBuilder.java`.
2. Follow the steps as outlined in the comments.

## 2. SeasonBuilder

1. Open the file `SeasonBuilder.java`.
2. Follow the steps as outlined in the comments.

## 3. TeamBuilder

1. Open the file `TeamBuilder.java`.
2. Follow the steps as outlined in the comments.
