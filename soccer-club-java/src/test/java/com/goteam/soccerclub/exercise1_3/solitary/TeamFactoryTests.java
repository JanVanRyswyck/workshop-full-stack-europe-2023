package com.goteam.soccerclub.exercise1_3.solitary;

import com.goteam.soccerclub.common.ContextSpecification;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class TeamFactoryTests {

    // The happy path scenario.
    // Here we're going to observe whether a `Team` is correctly created by the `TeamFactory`.
    @Nested
    class When_creating_a_new_team
        extends ContextSpecification {

        @Override
        protected void establishContext() {

            // Arrange stage
            // Create an instance of a `TeamFactory` (SUT) and everything that is necessary for executing the scenario.
        }

        @Override
        protected void because() {

            // Act stage
            // Execute the `createTeam` method on the `TeamFactory` instance created in `establishContext`.
        }

        @Test
        public void then_a_new_team_should_be_created() {

            // Assert stage
            // The `createTeam` method should return a pair, with a newly created `Team` on the left and a null
            // reference on the right. Instead of writing an assert statement for each individual property, use the
            // `usingRecursiveComparison` to verify the result.
            // e.g. `assertThat(result).usingRecursiveComparison().isEqualTo(expectedResult)`
            //
            // Tip: You can use the test data builders for initializing the expected `Team` instance.
        }
    }

    // Edge case #1
    // The soccer association imposes on clubs that a team can only be composed after the end of one season and the
    // beginning of a new season (between May 1 and August 31). When the current date is before the allowed composition
    // period, then the `createTeam` method should indicate a failed operation by returning that domain violation.
    @Nested
    class When_creating_a_new_team_before_the_start_of_the_authorised_composition_period
        extends ContextSpecification {

        @Override
        protected void establishContext() {

            // Arrange stage
            // Create an instance of a `TeamFactory` (SUT) and everything that is necessary for executing the scenario.
            // The `getCurrentDate` method of the test double for `Clock` should return April 30.
        }

        @Override
        protected void because() {

            // Act stage
            // Execute the `createTeam` method on the `TeamFactory` instance created in `establishContext`.
        }

        @Test
        public void then_it_should_indicate_a_failure() {

            // Assert stage
            // The `createTeam` method should return a pair, with a null reference on the left and a domain violation
            // on the right. Instead of writing an assert statement for each individual property, use the
            // `usingRecursiveComparison` to verify the result.
            // e.g. `assertThat(result).usingRecursiveComparison().isEqualTo(expectedResult)`
            //
            // Tip: The `TEAM_COMPOSITION_PERIOD_VIOLATION` static field can be used as the domain violation message.
        }
    }

    // Edge case #2
    // Here we're going to verify the same rule as described for the previous scenario. This time, when the current date
    // is after the allowed composition period of a team, then the `createTeam` method should indicate a failed operation
    // by returning that domain violation.
    @Nested
    class When_creating_a_new_team_after_the_end_of_the_authorised_composition_period
        extends ContextSpecification {

        @Override
        protected void establishContext() {

            // Arrange stage
            // Create an instance of a `TeamFactory` (SUT) and everything that is necessary for executing the scenario.
            // The `getCurrentDate` method of the test double for `Clock` should return September 1.
        }

        @Override
        protected void because() {

            // Act stage
            // Execute the `createTeam` method on the `TeamFactory` instance created in `establishContext`.
        }

        @Test
        public void then_it_should_indicate_a_failure() {

            // Assert stage
            // The `createTeam` method should return a pair, with a null reference on the left and a domain violation
            // on the right. Instead of writing an assert statement for each individual property, use the
            // `usingRecursiveComparison` to verify the result.
            // e.g. `assertThat(result).usingRecursiveComparison().isEqualTo(expectedResult)`
            //
            // Tip: The `TEAM_COMPOSITION_PERIOD_VIOLATION` static field can be used as the domain violation message.
        }
    }
}
